package com.stary.pay.alipay.rules;
/**
 * <p>支付宝交易状态枚举</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-11-1
 */
public enum AlipayTradeStatusType {
	/**
	 * 交易创建，等待买家付款
	 */
	WAIT_BUYER_PAY("WAIT_BUYER_PAY","待付款"),
	/**
	 * 未付款交易超时关闭，或支付完成后全额退款
	 */
	TRADE_CLOSED("TRADE_CLOSED","已关闭"),
	/**
	 * 交易支付成功
	 */
	TRADE_SUCCESS("TRADE_SUCCESS","已支付"),
	/**
	 * 交易结束，不可退款
	 */
	TRADE_FINISHED("TRADE_FINISHED","已完成");

	/**
	 * 状态
	 */
	private String status;
	/**
	 * 描述
	 */
	private String desc;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	private AlipayTradeStatusType(String status, String desc) {
		this.status = status;
		this.desc = desc;
	}
	
}
