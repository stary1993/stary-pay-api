package com.stary.pay.alipay.dto.request;

import java.io.Serializable;

/**
 * <p>支付宝订单同步基本请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-5-7
 */
public class AlipayOrderSync implements Serializable {

	private static final long serialVersionUID = 6962706928205222571L;
	
	/**
	 * 支付宝交易号
	 */
	private String trade_no;
	/**
	 * 标识一笔交易多次请求，同一笔交易多次信息同步时需要保证唯一 
	 */
	private String out_request_no;
	/**
	 * 易信息同步对应的业务类型，具体值与支付宝约定；<br/>
	 * 信用授权场景下传CREDIT_AUTH<br/>
	 * 信用代扣场景下传CREDIT_DEDUCT
	 */
	private String biz_type;
	/**
	 * 商户传入同步信息，具体值要和支付宝约定；用于芝麻信用租车、单次授权等信息同步场景，格式为json格式。（可选）
	 */
	private String order_biz_info;
	
	public String getTrade_no() {
		return trade_no;
	}
	
	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}
	
	public String getOut_request_no() {
		return out_request_no;
	}
	
	public void setOut_request_no(String out_request_no) {
		this.out_request_no = out_request_no;
	}
	
	public String getBiz_type() {
		return biz_type;
	}
	
	public void setBiz_type(String biz_type) {
		this.biz_type = biz_type;
	}
	
	public String getOrder_biz_info() {
		return order_biz_info;
	}
	
	public void setOrder_biz_info(String order_biz_info) {
		this.order_biz_info = order_biz_info;
	}
		
}
