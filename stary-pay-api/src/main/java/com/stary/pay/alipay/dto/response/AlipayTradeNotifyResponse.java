package com.stary.pay.alipay.dto.response;

import java.io.Serializable;

/**
 * <p>支付宝交易通知响应信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-10-31
 */
public class AlipayTradeNotifyResponse implements Serializable {
	
	private static final long serialVersionUID = 3037961378837744740L;
	
	/**
	 * 支付宝分配给开发者的应用ID
	 */
	private String appId;
	/**
	 * 接口名称
	 */
	private String method;
	/**
	 * 签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
	 */
	private String signType;
	/**
	 * 支付宝对本次支付结果的签名
	 */
	private String sign;
	/**
	 * 编码格式
	 */
	private String charset;
	/**
	 * 前台回跳的时间
	 */
	private String timestamp;
	/**
	 * 调用的接口版本，固定为：1.0
	 */
	private String version;
	/**
	 * 授权方的appid注：由于本接口暂不开放第三方应用授权，因此auth_app_id=app_id
	 */
	private String authAppId;
	/**
	 * 通知的发送时间
	 */
	private String notifyTime;
	/**
	 * 通知的类型 trade_status_sync
	 */
	private String notifyType;
	/**
	 * 通知校验ID
	 */
	private String notifyId;
	/**
	 * 商户网站唯一订单号
	 */
	private String outTradeNo;
	/**
	 * 该交易在支付宝系统中的交易流水号
	 */
	private String tradeNo;
	/**
	 * 交易目前所处的状态<br>
	 * WAIT_BUYER_PAY 交易创建，等待买家付款<br>
	 * TRADE_CLOSED 未付款交易超时关闭，或支付完成后全额退款<br>
	 * TRADE_SUCCESS 交易支付成功<br>
	 * TRADE_FINISHED 交易结束，不可退款
	 */
	private String tradeStatus;
	/**
	 * 该笔订单的资金总额
	 */
	private String totalAmount;
	/**
	 * 实收金额
	 */
	private String receiptAmount;
	/**
	 * 付款金额
	 */
	private String buyerPayAmount;	
	/**
	 * 交易创建时间
	 */
	private String gmtCreate;
	/**
	 * 交易付款时间
	 */
	private String gmtPayment;
	/**
	 * 交易结束时间
	 */
	private String gmtClose;
	/**
	 * 公共回传参数
	 */
	private String passbackParams;
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	public String getSignType() {
		return signType;
	}
	
	public void setSignType(String signType) {
		this.signType = signType;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getCharset() {
		return charset;
	}
	
	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getAuthAppId() {
		return authAppId;
	}
	
	public void setAuthAppId(String authAppId) {
		this.authAppId = authAppId;
	}
	
	public String getNotifyTime() {
		return notifyTime;
	}
	
	public void setNotifyTime(String notifyTime) {
		this.notifyTime = notifyTime;
	}
	
	public String getNotifyType() {
		return notifyType;
	}
	
	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}
	
	public String getNotifyId() {
		return notifyId;
	}
	
	public void setNotifyId(String notifyId) {
		this.notifyId = notifyId;
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	
	public String getTradeNo() {
		return tradeNo;
	}
	
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	
	public String getTradeStatus() {
		return tradeStatus;
	}
	
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public String getReceiptAmount() {
		return receiptAmount;
	}
	
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	
	public String getBuyerPayAmount() {
		return buyerPayAmount;
	}
	
	public void setBuyerPayAmount(String buyerPayAmount) {
		this.buyerPayAmount = buyerPayAmount;
	}
	
	public String getGmtCreate() {
		return gmtCreate;
	}
	
	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	public String getGmtPayment() {
		return gmtPayment;
	}
	
	public void setGmtPayment(String gmtPayment) {
		this.gmtPayment = gmtPayment;
	}
	
	public String getGmtClose() {
		return gmtClose;
	}
	
	public void setGmtClose(String gmtClose) {
		this.gmtClose = gmtClose;
	}
	
	public String getPassbackParams() {
		return passbackParams;
	}
	
	public void setPassbackParams(String passbackParams) {
		this.passbackParams = passbackParams;
	}		
	
}
