package com.stary.pay.alipay.dto.response;

import java.io.Serializable;

/**
 * <p>页面回跳return_url响应信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-10-31
 */
public class AlipayReturnUrlResponse implements Serializable {

	private static final long serialVersionUID = 5026536851470607121L;
	
	/**
	 * 支付宝分配给开发者的应用ID
	 */
	private String appId;
	/**
	 * 接口名称
	 */
	private String method;
	/**
	 * 签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
	 */
	private String signType;
	/**
	 * 支付宝对本次支付结果的签名
	 */
	private String sign;
	/**
	 * 编码格式
	 */
	private String charset;
	/**
	 * 前台回跳的时间
	 */
	private String timestamp;
	/**
	 * 调用的接口版本，固定为：1.0
	 */
	private String version;
	/**
	 * 授权方的appid注：由于本接口暂不开放第三方应用授权，因此auth_app_id=app_id
	 */
	private String authAppId;
	/**
	 * 商户网站唯一订单号
	 */
	private String outTradeNo;
	/**
	 * 该交易在支付宝系统中的交易流水号
	 */
	private String tradeNo;
	/**
	 * 该笔订单的资金总额
	 */
	private String totalAmount;
	/**
	 * 收款支付宝账号对应的支付宝唯一用户号
	 */
	private String sellerId;
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getSignType() {
		return signType;
	}
	public void setSignType(String signType) {
		this.signType = signType;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAuthAppId() {
		return authAppId;
	}
	public void setAuthAppId(String authAppId) {
		this.authAppId = authAppId;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getSellerId() {
		return sellerId;
	}
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
		
}
