package com.stary.pay.alipay.config;


/**
 * <p>支付宝配置信息接口</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-10-10
 */
public interface IAlipayConfig {
	
	/**
	 * 获取支付宝分配给开发者的应用ID
	 * @return appid
	 */
	public String appId();
	/**
	 * 获取卖家支付宝用户ID
	 * @return seller_id
	 */
	public String sellerId();
	/**
	 * 获取卖家支付宝账号
	 * @return seller_id
	 */
	public String sellerEmail();
	/**
	 * 获取合作者身份ID
	 * @return partner_id
	 */
	public String partnerId();
	/**
	 * 获取 privateKey（应用公钥）
	 * @return privateKey
	 */
	public String privateKey();
	/**
	 * 获取 alipayPublicKey（支付宝公钥）
	 * @return alipayPublicKey
	 */
	public String alipayPublicKey();
	/**
	 * 获取encryptKey（AES密钥）
	 * @return encryptKey
	 */
	public String encryptKey();
	/**
	 * 获取 支付回调异步通知地址 POST请求
	 * @return notify_url
	 */
	public String notifyUrl();
	/**
	 * 获取获取 支付回调同步通知地址
	 * @return return_url
	 */
	public String returnUrl();

}
