package com.stary.pay.alipay.dto.request;

import java.io.Serializable;

/**
 * <p>统一下单基本请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2018-10-11
 */
public class AlipayUnifiedorder implements Serializable {
	
	private static final long serialVersionUID = 8595675433687995599L;
		
	/**
	 * 商户订单号（必填）
	 */
	private String out_trade_no;
	/**
	 * 商品描述（必填）
	 */
	private String body;
	/**
	 * 销售产品码，与支付宝签约的产品码名称。 注：目前仅支持FAST_INSTANT_TRADE_PAY
	 */
	private String product_code = "FAST_INSTANT_TRADE_PAY";
	/**
	 * 订单总金额，单位为元（必填）
	 */
	private String total_amount;
	/**
	 * 订单标题（必填）
	 */
	private String subject;	
	/**
	 * 公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。<br>
	 * 支付宝只会在异步通知时将该参数原样返回。<br>
	 * 本参数必须进行UrlEncode之后才可以发送给支付宝。<br>
	 * （非必填）
	 */
	private String passback_params;
	/**
	 * 商品主类型：0 虚拟类商品，1 实物类商品（默认）注：虚拟类商品不支持使用花呗渠道
	 */
	private String goods_type = "1";
	/**
	 * 可用渠道，用户只能在指定渠道范围内支付，当有多个渠道时用“,”分隔 <br/>
	 * 如：pcredit,moneyFund,debitCardExpress
	 */
	private String enable_pay_channels;
	/**
	 * 禁用渠道，用户不可用指定渠道支付，当有多个渠道时用“,”分隔<br>
	 * 如：pcredit,moneyFund,debitCardExpress
	 */
	private String disable_pay_channels;
	
	public String getOut_trade_no() {
		return out_trade_no;
	}
	
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public String getProduct_code() {
		return product_code;
	}
	
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	
	public String getTotal_amount() {
		return total_amount;
	}
	
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getPassback_params() {
		return passback_params;
	}
	
	public void setPassback_params(String passback_params) {
		this.passback_params = passback_params;
	}
	
	public String getGoods_type() {
		return goods_type;
	}
	
	public void setGoods_type(String goods_type) {
		this.goods_type = goods_type;
	}
	
	public String getEnable_pay_channels() {
		return enable_pay_channels;
	}
	
	public void setEnable_pay_channels(String enable_pay_channels) {
		this.enable_pay_channels = enable_pay_channels;
	}
	
	public String getDisable_pay_channels() {
		return disable_pay_channels;
	}
	
	public void setDisable_pay_channels(String disable_pay_channels) {
		this.disable_pay_channels = disable_pay_channels;
	}
		
}
