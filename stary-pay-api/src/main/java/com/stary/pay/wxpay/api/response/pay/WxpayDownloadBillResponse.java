package com.stary.pay.wxpay.api.response.pay;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay downloadbill response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-28
 */
public class WxpayDownloadBillResponse extends WxpayResponse {

	private static final long serialVersionUID = 5822703842125393251L;
	
	/**
	 * 成功时，数据以文本表格的方式返回，第一行为表头，后面各行为对应的字段内容，字段内容跟查询订单或退款结果一致，具体字段说明可查阅相应接口。 
	 */
	private String data;	
	/**
	 * 失败错误码
	 */
	private String errorCode;
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
		
}
