package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

/**
 * <p>微信交易保障业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-11
 */
public class WxpayReport implements Serializable {

	private static final long serialVersionUID = 1212783737647811467L;
	
	/**
	 * 微信支付分配的终端设备号，商户自定义
	 */
	private String device_info;
	/**
	 * 接口URL，刷卡支付终端上报统一填：https://api.mch.weixin.qq.com/pay/batchreport/micropay/total
	 */
	private String interface_url;
	/**
	 * 发起接口调用时的机器IP
	 */
	private String user_ip;
	/**
	 * 上报数据包
	 */
	private String trades;
	
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getInterface_url() {
		return interface_url;
	}
	public void setInterface_url(String interface_url) {
		this.interface_url = interface_url;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getTrades() {
		return trades;
	}
	public void setTrades(String trades) {
		this.trades = trades;
	}		
	
}
