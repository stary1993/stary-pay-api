package com.stary.pay.wxpay.api.request.pay;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.pay.WxpayCloseOrderResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay close order request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-28
 */
public class WxpayCloseOrderRequest implements WxpayRequest<WxpayCloseOrderResponse> {

	/**
	 * 关闭订单接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayCloseOrderResponse> getResponseClass() {
		return WxpayCloseOrderResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return false;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
