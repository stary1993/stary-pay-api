package com.stary.pay.wxpay.dto.response;

import java.io.Serializable;

/**
 * <p>微信小程序 用户手机号码信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-30
 * @version 1.3.0
 */
public class WxMiniProgramPhoneNumber implements Serializable {


	private static final long serialVersionUID = 1L;

	/**
	 * 用户绑定的手机号（国外手机号会有区号）
	 */
	private String phoneNumber;
	/**
	 * 没有区号的手机号
	 */
	private String purePhoneNumber;
	/**
	 * 区号
	 */
	private String countryCode;
	/**
	 * 水印
	 */
	private WxMiniProgramWatermark watermark;
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPurePhoneNumber() {
		return purePhoneNumber;
	}
	public void setPurePhoneNumber(String purePhoneNumber) {
		this.purePhoneNumber = purePhoneNumber;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public WxMiniProgramWatermark getWatermark() {
		return watermark;
	}
	public void setWatermark(WxMiniProgramWatermark watermark) {
		this.watermark = watermark;
	}



}
