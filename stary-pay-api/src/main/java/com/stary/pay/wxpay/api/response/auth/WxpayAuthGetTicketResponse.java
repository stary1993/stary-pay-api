package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayAuthResponse;

/**
 * <p>wxpayauth get ticket resonse</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public class WxpayAuthGetTicketResponse extends WxpayAuthResponse {

	private static final long serialVersionUID = 9115421163416779657L;
	
	/**
	 * 临时票据
	 */
	private String ticket;
	/**
	 * 有效期7200秒，开发者必须在自己的服务全局缓存
	 */
	private String expiresIn;
	
	public String getTicket() {
		return ticket;
	}
	
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
	public String getExpiresIn() {
		return expiresIn;
	}
	
	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
	
}
