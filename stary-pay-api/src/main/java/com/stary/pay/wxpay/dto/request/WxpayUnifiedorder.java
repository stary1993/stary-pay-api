package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

import com.stary.pay.wxpay.api.rules.WxpayTradeType;

/**
 * <p>微信统一下单业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-11
 */
public class WxpayUnifiedorder implements Serializable {

	private static final long serialVersionUID = 2380554180818897542L;

	/**
	 * 商品描述
	 */
	private String body;
	/**
	 * 商户订单号
	 */
	private String out_trade_no;
	/**
	 * 订单总金额，单位为分
	 */
	private String total_fee;
	/**
	 * 终端IP
	 */
	private String spbill_create_ip;
	/**
	 * 交易类型 取值如下：JSAPI，NATIVE，APP等
	 */
	private WxpayTradeType trade_type;
	/**
	 * 用户标识  trade_type=JSAPI时（即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识
	 */
	private String openid;
	/**
	 * 子公众号用户标识
	 */
	private String sub_openid;
	/**
	 * 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用。
	 */
	private String attach;
	/**
	 * 自定义参数，可以为终端设备号(门店号或收银设备ID)，PC网页或公众号内支付可以传"WEB"
	 */
	private String device_info = "WEB";
	/**
	 * 商品详细描述
	 */
	private String detail;
	/**
	 * 订单生成时间，格式为yyyyMMddHHmmss
	 */
	private String time_start;
	/**
	 * 订单失效时间，格式为yyyyMMddHHmmss
	 */
	private String time_expire;
	/**
	 * 订单优惠标记，使用代金券或立减优惠功能时需要的参数
	 */
	private String goods_tag;
	/**
	 * 上传此参数no_credit--可限制用户不能使用信用卡支付
	 */
	private String limit_pay;
	/**
	 * 电子发票入口开放标识<br/>
	 * Y，传入Y时，支付成功消息和支付详情页将出现开票入口。<br/>
	 * 需要在微信支付商户平台或微信公众平台开通电子发票功能，传此字段才可生效。<br/>
	 */
	private String receipt;
	/**
	 * trade_type=NATIVE时，此参数必传。此参数为二维码中包含的商品ID，商户自行定义。
	 */
	private String product_id;
	/**
	 * 场景信息<br/>
	 * 该字段常用于线下活动时的场景信息上报，支持上报实际门店信息，商户也可以按需求自己上报相关信息。<br/>
	 * 该字段为JSON对象数据，对象格式为{"store_info":{"id": "门店ID","name": "名称","area_code": "所在地行政区划码","address": "详细地址" }}
	 */
	private String scene_info;

	public WxpayUnifiedorder() {
		super();
	}
	/**
	 * 扫码支付
	 * @param body
	 * @param out_trade_no
	 * @param total_fee
	 * @param spbill_create_ip
	 */
	public WxpayUnifiedorder(String body, String out_trade_no,
			String total_fee, String spbill_create_ip) {
		super();
		this.body = body;
		this.out_trade_no = out_trade_no;
		this.total_fee = total_fee;
		this.spbill_create_ip = spbill_create_ip;
		this.trade_type = WxpayTradeType.NATIVE;
	}

	/**
	 * JSAPI支付
	 * @param body
	 * @param out_trade_no
	 * @param total_fee
	 * @param spbill_create_ip
	 * @param openid
	 */
	public WxpayUnifiedorder(String body, String out_trade_no,
			String total_fee, String spbill_create_ip, String openid) {
		super();
		this.body = body;
		this.out_trade_no = out_trade_no;
		this.total_fee = total_fee;
		this.spbill_create_ip = spbill_create_ip;
		this.trade_type =  WxpayTradeType.JSAPI;
		this.openid = openid;
	}

	public WxpayUnifiedorder(String body, String out_trade_no,
			String total_fee, String spbill_create_ip, WxpayTradeType trade_type) {
		super();
		this.body = body;
		this.out_trade_no = out_trade_no;
		this.total_fee = total_fee;
		this.spbill_create_ip = spbill_create_ip;
		this.trade_type = trade_type;
	}

	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}
	public WxpayTradeType getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(WxpayTradeType trade_type) {
		this.trade_type = trade_type;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSub_openid() {
		return sub_openid;
	}
	public void setSub_openid(String sub_openid) {
		this.sub_openid = sub_openid;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getTime_start() {
		return time_start;
	}
	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}
	public String getTime_expire() {
		return time_expire;
	}
	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}
	public String getGoods_tag() {
		return goods_tag;
	}
	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}
	public String getLimit_pay() {
		return limit_pay;
	}
	public void setLimit_pay(String limit_pay) {
		this.limit_pay = limit_pay;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getScene_info() {
		return scene_info;
	}
	public void setScene_info(String scene_info) {
		this.scene_info = scene_info;
	}

}
