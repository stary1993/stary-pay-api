package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

/**
 * <p>查询代金券信息业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-14
 */
public class WxpayQueryCouponsInfo implements Serializable {

	private static final long serialVersionUID = 4047537976047422115L;

	/**
	 * 代金券id
	 */
	private String coupon_id;
	/**
	 * Openid信息，用户在appid下的openid
	 */
	private String openid;
	/**
	 * 代金劵对应的批次号
	 */
	private String stock_id;
	/**
	 * 操作员帐号, 默认为商户号，可在商户平台配置操作员对应的api权限
	 */
	private String op_user_id;
	/**
	 * 微信支付分配的终端设备号
	 */
	private String device_info;

	public WxpayQueryCouponsInfo() {
		super();
	}

	public WxpayQueryCouponsInfo(String coupon_id, String openid,
			String stock_id) {
		super();
		this.coupon_id = coupon_id;
		this.openid = openid;
		this.stock_id = stock_id;
	}

	public WxpayQueryCouponsInfo(String coupon_id, String openid,
			String stock_id, String op_user_id, String device_info) {
		super();
		this.coupon_id = coupon_id;
		this.openid = openid;
		this.stock_id = stock_id;
		this.op_user_id = op_user_id;
		this.device_info = device_info;
	}

	public String getCoupon_id() {
		return coupon_id;
	}
	public void setCoupon_id(String coupon_id) {
		this.coupon_id = coupon_id;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getStock_id() {
		return stock_id;
	}
	public void setStock_id(String stock_id) {
		this.stock_id = stock_id;
	}
	public String getOp_user_id() {
		return op_user_id;
	}
	public void setOp_user_id(String op_user_id) {
		this.op_user_id = op_user_id;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

}
