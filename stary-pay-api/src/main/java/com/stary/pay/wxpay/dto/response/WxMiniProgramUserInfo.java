package com.stary.pay.wxpay.dto.response;

import java.io.Serializable;

/**
 * <p>微信小程序 用户信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-30
 * @version 1.3.0
 */
public class WxMiniProgramUserInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户唯一标识
	 */
	private String openId;
	/**
	 * 用户昵称
	 */
	private String nickName;
	/**
	 * 用户性别 0：未知、1：男、2：女
	 */
	private Integer gender;
	/**
	 * 用户所在城市
	 */
	private String city;
	/**
	 * 用户所在省份
	 */
	private String province;
	/**
	 * 用户所在国家
	 */
	private String country;
	/**
	 * 用户头像图片的 URL
	 */
	private String avatarUrl;
	/**
	 * unionId
	 */
	private String unionId;
	/**
	 * 水印
	 */
	private WxMiniProgramWatermark watermark;


	public String getOpenId() {
		return openId;
	}


	public void setOpenId(String openId) {
		this.openId = openId;
	}


	public String getNickName() {
		return nickName;
	}


	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	public Integer getGender() {
		return gender;
	}


	public void setGender(Integer gender) {
		this.gender = gender;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getAvatarUrl() {
		return avatarUrl;
	}


	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}


	public String getUnionId() {
		return unionId;
	}


	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}


	public WxMiniProgramWatermark getWatermark() {
		return watermark;
	}


	public void setWatermark(WxMiniProgramWatermark watermark) {
		this.watermark = watermark;
	}

}
