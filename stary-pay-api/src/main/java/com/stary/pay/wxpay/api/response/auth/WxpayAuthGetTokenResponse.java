package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayAuthResponse;

/**
 * <p>wxpayauth get token resonse</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public class WxpayAuthGetTokenResponse extends WxpayAuthResponse {

	private static final long serialVersionUID = 9115421163417779657L;
	/**
	 * 获取到的凭证
	 */
	private String accessToken;
	/**
	 * 凭证有效时间，单位：秒
	 */
	private String expiresIn;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
		
}
