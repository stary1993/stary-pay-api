package com.stary.pay.wxpay.api.response.tool;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>查询企业付款（转账）记录响应信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-12
 */
public class WxpayGetTransferInfoResponse extends WxpayResponse {


	private static final long serialVersionUID = 8620436418389964233L;
	
	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/
	
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;

	/*------------ 以下字段在return_code 、result_code都为SUCCESS时有返回 ------------*/
	
	/**
	 * 商户单号 
	 */
	private String partnerTradeNo;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 商户号的appid
	 */
	private String appid;
	/**
	 * 微信付款单号
	 */
	private String detailId;
	/**
	 * 转账状态 <br>
	 * SUCCESS：转账成功 <br>
	 * FAILED：转账失败 <br>
	 * PROCESSING：处理中 <br>
	 */
	private String status;
	/**
	 * 收款用户openid
	 */
	private String openid;
	/**
	 * 企业付款备注 
	 */
	private String desc;

	/**
	 * 付款金额，单位分
	 */
	private String paymentAmount;
	/**
	 * 付款失败原因
	 */
	private String reason;
	/**
	 * 转账时间 
	 */
	private String transferTime;
	/**
	 * 付款成功时间
	 */
	private String paymentTime;
	/**
	 * 收款用户姓名
	 */
	private String transferName;
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getPartnerTradeNo() {
		return partnerTradeNo;
	}
	
	public void setPartnerTradeNo(String partnerTradeNo) {
		this.partnerTradeNo = partnerTradeNo;
	}
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getDetailId() {
		return detailId;
	}
	
	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getOpenid() {
		return openid;
	}
	
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getPaymentAmount() {
		return paymentAmount;
	}
	
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getTransferTime() {
		return transferTime;
	}
	
	public void setTransferTime(String transferTime) {
		this.transferTime = transferTime;
	}
	
	public String getPaymentTime() {
		return paymentTime;
	}
	
	public void setPaymentTime(String paymentTime) {
		this.paymentTime = paymentTime;
	}
	
	public String getTransferName() {
		return transferName;
	}
	
	public void setTransferName(String transferName) {
		this.transferName = transferName;
	}
			
}
