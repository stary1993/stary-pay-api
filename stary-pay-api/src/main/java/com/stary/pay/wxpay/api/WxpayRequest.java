package com.stary.pay.wxpay.api;
import java.util.Map;

/**
 * <p>支付请求接口</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-10
 */
public interface WxpayRequest <T extends WxpayResponse> {

	/**
	 * 返回服务域名
	 * @return 服务域名
	 */
	public String serverDomain();
	/**
	 * 是否需要证书
	 * @return true/false
	 */
	public boolean isNeedCert();
	/**
	 * 是否验证签名
	 * @return true/false
	 */
	public boolean isCheckSign();
	/**
	 * 返回通知地址
	 * @return 通知地址
	 */
	public String getNotifyUrl();
	/**
	 *  设置通知地址
	 * @param notifyUrl
	 */
	public void setNotifyUrl(String notifyUrl);
	/**
	 * 获取所有的Key-Value形式的文本请求参数集合。其中：
	 * <ul>
	 * <li>Key: 请求参数名</li>
	 * <li>Value: 请求参数值</li>
	 * </ul>
	 * @return 文本请求参数集合
	 */
	public Map<String, String> getTextParams();
	/**
	 * 得到当前API的响应结果类型
	 * @return 响应类型
	 */
	public Class<T> getResponseClass();

}
