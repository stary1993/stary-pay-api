package com.stary.pay.wxpay.api.request.auth;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayAuthRequest;
import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.response.auth.WxpayAuthCheckTokenResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpayauth check token request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public class WxpayAuthCheckTokenRequest implements WxpayAuthRequest<WxpayAuthCheckTokenResponse> {

	/**
	 * 检验授权凭证（access_token）是否有效接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayAuthCheckTokenResponse> getResponseClass() {
		return WxpayAuthCheckTokenResponse.class;
	}

}
