package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayJsApiResponse;

/**
 * <p>wxpay jsapi config response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-18
 */
public class WxpayJsApiConfigResponse extends WxpayJsApiResponse {

	private static final long serialVersionUID = 5812703842125393259L;
		
}
