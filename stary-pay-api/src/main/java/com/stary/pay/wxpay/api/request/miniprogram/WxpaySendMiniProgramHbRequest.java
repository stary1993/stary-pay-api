package com.stary.pay.wxpay.api.request.miniprogram;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.miniprogram.WxpaySendMiniProgramHbResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay sendminiprogramhb request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-29
 * @version 1.3.0
 */
public class WxpaySendMiniProgramHbRequest implements WxpayRequest<WxpaySendMiniProgramHbResponse> {
	/**
	 * 发放小程序红包接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpaySendMiniProgramHbResponse> getResponseClass() {
		return WxpaySendMiniProgramHbResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return true;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
