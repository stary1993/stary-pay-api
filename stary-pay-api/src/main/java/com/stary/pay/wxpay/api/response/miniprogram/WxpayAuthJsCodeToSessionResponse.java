package com.stary.pay.wxpay.api.response.miniprogram;

import com.stary.pay.wxpay.api.WxpayAuthResponse;

/**
 * <p>wxpay miniprogram auth jscode2session response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-29
 * @version 1.3.0
 */
public class WxpayAuthJsCodeToSessionResponse extends WxpayAuthResponse {

	private static final long serialVersionUID = 942049939513104121L;

	/**
	 * 用户唯一标识
	 */
	private String openid;
	/**
	 * 会话密钥
	 */
	private String sessionKey;
	/**
	 * 用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。
	 */
	private String unionid;


	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

}
