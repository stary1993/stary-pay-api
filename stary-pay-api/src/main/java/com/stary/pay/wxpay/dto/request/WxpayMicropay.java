package com.stary.pay.wxpay.dto.request;

import com.stary.pay.wxpay.api.rules.WxpayTradeType;

/**
 * <p>微信付款码支付业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-14
 */
public class WxpayMicropay extends WxpayUnifiedorder {

	private static final long serialVersionUID = 3980243882020227536L;
	/**
	 * 扫码支付授权码，设备读取用户微信中的条码或者二维码信息
	 * （注：用户付款码条形码规则：18位纯数字，以10、11、12、13、14、15开头）
	 */
	private String auth_code;
	
	public WxpayMicropay() {
		super();
	}
		
	/**
	 * 付款码（刷卡）支付
	 * @param body
	 * @param out_trade_no
	 * @param total_fee
	 * @param spbill_create_ip
	 * @param auth_code
	 */
	public WxpayMicropay(String body, String out_trade_no, String total_fee,
			String spbill_create_ip, String auth_code) {
		super(body, out_trade_no, total_fee, spbill_create_ip, WxpayTradeType.MICROPAY);
		this.auth_code = auth_code;
	}

	public String getAuth_code() {
		return auth_code;
	}
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}
	
}
