package com.stary.pay.wxpay.api;

/**
 * <p>微信业务异常类</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-10
 */
public class WxpayApiException extends Exception {

	private static final long serialVersionUID = 5144757309644981452L;

	public WxpayApiException() {
		super();
	}

	public WxpayApiException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public WxpayApiException(String message, Throwable cause) {
		super(message, cause);
	}

	public WxpayApiException(String message) {
		super(message);
	}

	public WxpayApiException(Throwable cause) {
		super(cause);
	}

}
