package com.stary.pay.wxpay.api.rules;
/**
 * <p>应用授权作用域枚举</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public enum WxpayAuthScopeType {
	/**
	 * 静默授权
	 */snsapi_base,
	 /**
	  * 用户手动授权
	  */
	 snsapi_userinfo;

}
