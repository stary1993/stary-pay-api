package com.stary.pay.wxpay.dto.response;

import java.io.Serializable;

/**
 * <p>微信小程序 用户水印信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-30
 * @version 1.3.0
 */
public class WxMiniProgramWatermark implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 小程序appid
	 */
	private String appid;
	/**
	 * 时间戳
	 */
	private String timestamp;

	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
