package com.stary.pay.wxpay.api.response.pay;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay refund notify response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpayRefundNotifyResponse extends WxpayResponse {

	private static final long serialVersionUID = 3290457041954325683L;
	
	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 特约商户公众账号ID
	 */
	private String subAppid;
	/**
	 * 特约商户商户号
	 */
	private String subMchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 加密信息请用商户秘钥进行解密
	 */
	private String reqInfo;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getSubAppid() {
		return subAppid;
	}

	public void setSubAppid(String subAppid) {
		this.subAppid = subAppid;
	}

	public String getSubMchId() {
		return subMchId;
	}

	public void setSubMchId(String subMchId) {
		this.subMchId = subMchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getReqInfo() {
		return reqInfo;
	}
	
	public void setReqInfo(String reqInfo) {
		this.reqInfo = reqInfo;
	}
	
}
