package com.stary.pay.wxpay.api.response.pay;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay pay notify response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpayPayNotifyResponse extends WxpayResponse {

	private static final long serialVersionUID = 3290457041954325683L;
	
	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 子商户公众账号ID
	 */
	private String subAppid;
	/**
	 * 子商户号
	 */
	private String subMchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 微信支付分配的终端设备号
	 */
	private String deviceInfo;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
	 */
	private String signType;
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;
	/**
	 * 用户标识
	 */
	private String openid;
	/**
	 * 用户在子商户appid下的唯一标识
	 */
	private String subOpenid;
	/**
	 * 用户是否关注公众账号，Y-关注，N-未关注（机构商户不返回）
	 */
	private String isSubscribe;
	/**
	 * 用户是否关注子公众账号，Y-关注，N-未关注（机构商户不返回）
	 */
	private String subIsSubscribe;
	/**
	 * 微信订单号
	 */
	private String transactionId;
	/**
	 * 商户订单号
	 */
	private String outTradeNo;
	/**
	 * 商家数据包，原样返回
	 */
	private String attach;
	/**
	 * 银行类型，采用字符串类型的银行标识
	 */
	private String bankType;
	/**
	 * 订单总金额，单位为分
	 */
	private String totalFee;
	/**
	 * 应结订单金额，应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额
	 */
	private String settlementTotalFee;		
	/**
	 * 现金支付金额，单位为分
	 */
	private String cashFee;
	/**
	 * 总代金券金额，代金券金额<=订单金额，订单金额-代金券金额=现金支付金额
	 */
	private String couponFee;
	/**
	 * 代金券使用数量
	 */
	private String couponCount;
	/**
	 * 支付完成时间,格式为yyyyMMddHHmmss
	 */
	private String timeEnd;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getSubAppid() {
		return subAppid;
	}

	public void setSubAppid(String subAppid) {
		this.subAppid = subAppid;
	}

	public String getSubMchId() {
		return subMchId;
	}

	public void setSubMchId(String subMchId) {
		this.subMchId = subMchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getSignType() {
		return signType;
	}
	
	public void setSignType(String signType) {
		this.signType = signType;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getOpenid() {
		return openid;
	}
	
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getSubOpenid() {
		return subOpenid;
	}

	public void setSubOpenid(String subOpenid) {
		this.subOpenid = subOpenid;
	}

	public String getIsSubscribe() {
		return isSubscribe;
	}
	
	public void setIsSubscribe(String isSubscribe) {
		this.isSubscribe = isSubscribe;
	}
	
	public String getSubIsSubscribe() {
		return subIsSubscribe;
	}

	public void setSubIsSubscribe(String subIsSubscribe) {
		this.subIsSubscribe = subIsSubscribe;
	}

	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	
	public String getAttach() {
		return attach;
	}
	
	public void setAttach(String attach) {
		this.attach = attach;
	}
	
	public String getBankType() {
		return bankType;
	}
	
	public void setBankType(String bankType) {
		this.bankType = bankType;
	}
	
	public String getTotalFee() {
		return totalFee;
	}
	
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	
	public String getSettlementTotalFee() {
		return settlementTotalFee;
	}
	
	public void setSettlementTotalFee(String settlementTotalFee) {
		this.settlementTotalFee = settlementTotalFee;
	}
	
	public String getCashFee() {
		return cashFee;
	}
	
	public void setCashFee(String cashFee) {
		this.cashFee = cashFee;
	}
	
	public String getCouponFee() {
		return couponFee;
	}
	
	public void setCouponFee(String couponFee) {
		this.couponFee = couponFee;
	}
	
	public String getCouponCount() {
		return couponCount;
	}
	
	public void setCouponCount(String couponCount) {
		this.couponCount = couponCount;
	}
	
	public String getTimeEnd() {
		return timeEnd;
	}
	
	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}
	
}
