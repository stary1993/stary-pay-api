package com.stary.pay.wxpay.api.request.miniprogram;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayAuthRequest;
import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.response.miniprogram.WxpayAuthJsCodeToSessionResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay miniprogram auth jscode2session request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-29
 * @version 1.3.0
 */
public class WxpayAuthJsCodeToSessionRequest implements WxpayAuthRequest< WxpayAuthJsCodeToSessionResponse> {

	/**
	 * 小程序登录凭证校验接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayAuthJsCodeToSessionResponse> getResponseClass() {
		return WxpayAuthJsCodeToSessionResponse.class;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API;
	}

}
