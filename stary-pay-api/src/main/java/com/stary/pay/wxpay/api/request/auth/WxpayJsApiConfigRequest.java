package com.stary.pay.wxpay.api.request.auth;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayJsApiRequest;
import com.stary.pay.wxpay.api.response.auth.WxpayJsApiConfigResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay jsapi config request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-18
 */
public class WxpayJsApiConfigRequest implements WxpayJsApiRequest<WxpayJsApiConfigResponse> {

	/**
	 * 微信JS-SDK 签名数据
	 */
	private String signatureData;

	public String getSignatureData() {
		return signatureData;
	}

	public void setSignatureData(String signatureData) {
		this.signatureData = signatureData;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.SIGNATURE_DATA, this.signatureData);
		return txtParams;
	}

	@Override
	public Class<WxpayJsApiConfigResponse> getResponseClass() {
		return WxpayJsApiConfigResponse.class;
	}

}
