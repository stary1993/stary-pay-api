package com.stary.pay.wxpay.api.request.tool;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.tool.WxpaySendCouponResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay send_coupon request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpaySendCouponRequest implements WxpayRequest<WxpaySendCouponResponse> {

	/**
	 * 发放代金券接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpaySendCouponResponse> getResponseClass() {
		return WxpaySendCouponResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return true;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
