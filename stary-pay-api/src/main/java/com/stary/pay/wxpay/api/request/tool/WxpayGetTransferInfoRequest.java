package com.stary.pay.wxpay.api.request.tool;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.tool.WxpayGetTransferInfoResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay gettransferinfo request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-12
 */
public class WxpayGetTransferInfoRequest implements WxpayRequest<WxpayGetTransferInfoResponse> {
	/**
	 * 查询企业付款接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayGetTransferInfoResponse> getResponseClass() {
		return WxpayGetTransferInfoResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return true;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
