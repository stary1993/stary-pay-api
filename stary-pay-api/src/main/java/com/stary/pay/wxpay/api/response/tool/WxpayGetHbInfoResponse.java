package com.stary.pay.wxpay.api.response.tool;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay gethbinfo response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-14
 */
public class WxpayGetHbInfoResponse extends WxpayResponse {

	private static final long serialVersionUID = 4881040160674770274L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;

	/*------------ 以下字段在return_code 、result_code都为SUCCESS时有返回 ------------*/

	/**
	 * 使用API发放现金红包时返回的红包单号 
	 */
	private String detailId;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 商户订单号
	 */
	private String mchBillno;
	/**
	 * 红包状态 <br/>
	 * SENDING:发放中 <br/>
	 * SENT:已发放待领取 <br/>
	 * FAILED：发放失败 <br/>
	 * RECEIVED:已领取 <br/>
	 * RFUND_ING:退款中 <br/>
	 * REFUND:已退款
	 */
	private String status;
	/**
	 * 发放类型 <br/>
	 * API:通过API接口发放 <br/>
	 * UPLOAD:通过上传文件方式发放 <br/>
	 * ACTIVITY:通过活动方式发放
	 */
	private String sendType;
	/**
	 * 红包类型 <br/> 
	 * GROUP:裂变红包 <br/>
	 * NORMAL:普通红包 
	 */
	private String hbType;
	/**
	 * 红包个数 
	 */
	private String totalNum;	
	/**
	 * 付款金额，单位分
	 */
	private String totalAmount;
	/**
	 * 发送失败原因
	 */
	private String reason;
	/**
	 * 红包发送时间 
	 */
	private String sendTime;
	/**
	 * 红包的退款时间（如果其未领取的退款）
	 */
	private String refundTime;
	/**
	 * 红包退款金额
	 */
	private String refundAmount;
	/**
	 * 祝福语 
	 */
	private String wishing;
	/**
	 * 活动描述，低版本微信可见 
	 */
	private String remark;
	/**
	 * 发红包的活动名称 
	 */
	private String actName;
	/**
	 * 裂变红包的领取列表
	 */
	private String hblist;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCodeDes() {
		return errCodeDes;
	}

	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getMchBillno() {
		return mchBillno;
	}

	public void setMchBillno(String mchBillno) {
		this.mchBillno = mchBillno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSendType() {
		return sendType;
	}

	public void setSendType(String sendType) {
		this.sendType = sendType;
	}

	public String getHbType() {
		return hbType;
	}

	public void setHbType(String hbType) {
		this.hbType = hbType;
	}

	public String getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getRefundTime() {
		return refundTime;
	}

	public void setRefundTime(String refundTime) {
		this.refundTime = refundTime;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getWishing() {
		return wishing;
	}

	public void setWishing(String wishing) {
		this.wishing = wishing;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName;
	}

	public String getHblist() {
		return hblist;
	}

	public void setHblist(String hblist) {
		this.hblist = hblist;
	}

}
