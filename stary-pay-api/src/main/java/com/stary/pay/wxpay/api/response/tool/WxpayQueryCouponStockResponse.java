package com.stary.pay.wxpay.api.response.tool;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay query_coupon_stock response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpayQueryCouponStockResponse extends WxpayResponse {

	private static final long serialVersionUID = 2278208556296512086L;
	
	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 微信支付分配的终端设备号
	 */
	private String deviceInfo;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;
	/**
	 * 代金券批次id
	 */
	private String couponStockId;
	/**
	 * 代金券名称
	 */
	private String couponName;
	/**
	 * 代金券面值,单位是分
	 */
	private String couponValue;
	/**
	 * 代金券使用最低限额,单位是分
	 */
	private String couponMininumn;
	/**
	 * 批次状态： 1-未激活；2-审批中；4-已激活；8-已作废；16-中止发放；
	 */
	private String couponStockStatus;
	/**
	 * 代金券数量
	 */
	private String couponTotal;
	/**
	 * 代金券每个人最多能领取的数量, 如果为0，则表示没有限制
	 */
	private String maxQuota;
	/**
	 * 代金券已经发送的数量
	 */
	private String isSendNum;
	/**
	 * 生效开始时间，格式为时间戳：1943787483
	 */
	private String beginTime;
	/**
	 * 生效结束时间，格式为时间戳：1943787490
	 */
	private String endTime;
	/**
	 * 创建时间，格式为时间戳：1943787420
	 */
	private String createTime;
	/**
	 * 代金券预算额度
	 */
	private String couponBudget;
	/**
	 * 生效开始时间，格式为年月日时间制：20181126152401
	 */
	private String beginTimeT;
	/**
	 * 生效结束时间，格式为年月日时间制：20181126152401
	 */
	private String endTimeT;
	/**
	 * 创建时间，格式为年月日时间制：20181126152401
	 */
	private String createTimeT;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getCouponStockId() {
		return couponStockId;
	}
	
	public void setCouponStockId(String couponStockId) {
		this.couponStockId = couponStockId;
	}
	
	public String getCouponName() {
		return couponName;
	}
	
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	
	public String getCouponValue() {
		return couponValue;
	}
	
	public void setCouponValue(String couponValue) {
		this.couponValue = couponValue;
	}
	
	public String getCouponMininumn() {
		return couponMininumn;
	}
	
	public void setCouponMininumn(String couponMininumn) {
		this.couponMininumn = couponMininumn;
	}
	
	public String getCouponStockStatus() {
		return couponStockStatus;
	}
	
	public void setCouponStockStatus(String couponStockStatus) {
		this.couponStockStatus = couponStockStatus;
	}
	
	public String getCouponTotal() {
		return couponTotal;
	}
	
	public void setCouponTotal(String couponTotal) {
		this.couponTotal = couponTotal;
	}
	
	public String getMaxQuota() {
		return maxQuota;
	}
	
	public void setMaxQuota(String maxQuota) {
		this.maxQuota = maxQuota;
	}
	
	public String getIsSendNum() {
		return isSendNum;
	}
	
	public void setIsSendNum(String isSendNum) {
		this.isSendNum = isSendNum;
	}
	
	public String getBeginTime() {
		return beginTime;
	}
	
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	public String getCouponBudget() {
		return couponBudget;
	}
	
	public void setCouponBudget(String couponBudget) {
		this.couponBudget = couponBudget;
	}
	
	public String getBeginTimeT() {
		return beginTimeT;
	}
	
	public void setBeginTimeT(String beginTimeT) {
		this.beginTimeT = beginTimeT;
	}
	
	public String getEndTimeT() {
		return endTimeT;
	}
	
	public void setEndTimeT(String endTimeT) {
		this.endTimeT = endTimeT;
	}
	
	public String getCreateTimeT() {
		return createTimeT;
	}
	
	public void setCreateTimeT(String createTimeT) {
		this.createTimeT = createTimeT;
	}
		
}
