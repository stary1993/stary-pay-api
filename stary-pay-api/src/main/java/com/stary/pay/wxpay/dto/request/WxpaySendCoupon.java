package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

/**
 * <p>微信发放代金券业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-14
 */
public class WxpaySendCoupon implements Serializable {
	
	private static final long serialVersionUID = 3355524380256241190L;
	
	/**
	 * 代金券批次id
	 */
	private String coupon_stock_id;
	/**
	 * openid记录数（目前支持num=1）
	 */
	private String openid_count = "1";
	/**
	 * 商户此次发放凭据号（格式：商户id+日期+流水号），商户侧需保持唯一性
	 */
	private String partner_trade_no;
	/**
	 * 用户openid
	 */
	private String openid;
	/**
	 * 操作员帐号，默认为商户号，可在商户平台配置操作员对应的api权限
	 */
	private String op_user_id;
	/**
	 * 微信支付分配的终端设备号
	 */
	private String device_info;
	
	public WxpaySendCoupon() {
		super();
	}
		
	public WxpaySendCoupon(String coupon_stock_id, String partner_trade_no, String openid) {
		super();
		this.coupon_stock_id = coupon_stock_id;
		this.partner_trade_no = partner_trade_no;
		this.openid = openid;
	}


	public WxpaySendCoupon(String coupon_stock_id, String partner_trade_no,
			String openid, String op_user_id, String device_info) {
		super();
		this.coupon_stock_id = coupon_stock_id;
		this.partner_trade_no = partner_trade_no;
		this.openid = openid;
		this.op_user_id = op_user_id;
		this.device_info = device_info;
	}

	public String getCoupon_stock_id() {
		return coupon_stock_id;
	}
	public void setCoupon_stock_id(String coupon_stock_id) {
		this.coupon_stock_id = coupon_stock_id;
	}
	public String getOpenid_count() {
		return openid_count;
	}
	public void setOpenid_count(String openid_count) {
		this.openid_count = openid_count;
	}
	public String getPartner_trade_no() {
		return partner_trade_no;
	}
	public void setPartner_trade_no(String partner_trade_no) {
		this.partner_trade_no = partner_trade_no;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getOp_user_id() {
		return op_user_id;
	}
	public void setOp_user_id(String op_user_id) {
		this.op_user_id = op_user_id;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	
}
