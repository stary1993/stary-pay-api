package com.stary.pay.wxpay.config;

/**
 * <p>微信公众号/商户号配置信息接口</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-10
 */
public interface IWxpayConfig {

	/**
	 * 获取 appid（应用ID）
	 * @return appid
	 */
	public String appid();
	/**
	 * 获取 sub_appid（子应用ID）
	 * @return appid
	 */
	public String subAppid();
	/**
	 * 获取 appsecret（应用秘钥）
	 * @return secret
	 */
	public String secret();
	/**
	 * 获取 mchid（商户ID）
	 * @return mch_id
	 */
	public String mchId();
	/**
	 * 获取 sub_mch_id（子商户ID）
	 * @return sub_mch_id
	 */
	public String subMchId();
	/**
	 * 获取 Key(支付秘钥)
	 * @return Key
	 */
	public String key();
	/**
	 * 获取子商户号Key(支付秘钥)
	 * @return Key
	 */
	public String subKey();

	/**
	 * 获取 支付回调异步通知地址
	 * @return notify_url
	 */
	public String notifyUrl();
	/**
	 * 获取 商户证书
	 * @return certFile
	 */
	public String certFile();
	/**
	 * 获取 子商户证书
	 * @return subCertFile
	 */
	public String subCertFile();
	/**
	 * 退款异步通知地址<br>
	 * 如果参数中传了notify_url，则商户平台上配置的回调地址将不会生效。
	 * @return refund notify_url
	 */
	public String refundNotifyUrl();
}
