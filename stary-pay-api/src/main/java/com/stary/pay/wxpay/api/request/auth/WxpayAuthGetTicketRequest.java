package com.stary.pay.wxpay.api.request.auth;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayAuthRequest;
import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.response.auth.WxpayAuthGetTicketResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpayauth get ticket request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public class WxpayAuthGetTicketRequest implements WxpayAuthRequest<WxpayAuthGetTicketResponse> {

	/**
	 * 微信JS接口的临时票据接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayAuthGetTicketResponse> getResponseClass() {
		return WxpayAuthGetTicketResponse.class;
	}

}
