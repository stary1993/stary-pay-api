package com.stary.pay.wxpay.api.response.pay;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay unifiedorder response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-11
 */
public class WxpayUnifiedorderResponse extends WxpayResponse {

	private static final long serialVersionUID = 942049939513104128L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 设备号
	 */
	private String deviceInfo;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 签名
	 */
	private String sign;
	
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;

	/*------------ 以下字段在return_code 和result_code都为SUCCESS的时候有返回 ------------*/

	/**
	 * 交易类型
	 */
	private String tradeType;
	/**
	 * 预支付交易会话标识 微信生成的预支付会话标识，用于后续接口调用中使用，该值有效期为2小时
	 */
	private String prepayId;
	/**
	 * 二维码链接 trade_type为NATIVE时有返回，用于生成二维码，展示给用户进行扫码支付
	 */
	private String codeUrl;
	/**
	 * H5支付跳转链接
	 */
	private String mwebUrl;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getTradeType() {
		return tradeType;
	}
	
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
	public String getPrepayId() {
		return prepayId;
	}
	
	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}
	
	public String getCodeUrl() {
		return codeUrl;
	}
	
	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}
	
	public String getMwebUrl() {
		return mwebUrl;
	}
	
	public void setMwebUrl(String mwebUrl) {
		this.mwebUrl = mwebUrl;
	}
	
}
