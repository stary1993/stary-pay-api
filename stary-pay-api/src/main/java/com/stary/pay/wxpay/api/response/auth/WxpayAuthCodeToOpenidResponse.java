package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay authcodetoopenid response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public class WxpayAuthCodeToOpenidResponse extends WxpayResponse {

	private static final long serialVersionUID = 942049939513104128L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 业务结果，SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 <br/>
	 * SYSTEMERROR--系统错误 <br/>
	 * AUTHCODEEXPIRE---授权码过期 <br/>
	 * AUTH_CODE_ERROR—授权码错误 <br/>
	 * AUTH_CODE_INVALID—授权码检验错误
	 */
	private String errCode;
	/**
	 * 错误代码描述
	 */
	private String errCodeDes;

	/*------------ 以下字段在return_code 和result_code都为SUCCESS的时候有返回  ------------*/

	/**
	 * 用户在商户appid下的唯一标识
	 */
	private String openid;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCodeDes() {
		return errCodeDes;
	}

	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

}
