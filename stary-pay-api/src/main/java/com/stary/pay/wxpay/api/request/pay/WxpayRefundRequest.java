package com.stary.pay.wxpay.api.request.pay;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.pay.WxpayRefundResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay refund request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-15
 */
public class WxpayRefundRequest implements WxpayRequest<WxpayRefundResponse> {

	/**
	 * 申请退款接口
	 */
	private String bizContent;

	private String notifyUrl;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayRefundResponse> getResponseClass() {
		return WxpayRefundResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return true;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
