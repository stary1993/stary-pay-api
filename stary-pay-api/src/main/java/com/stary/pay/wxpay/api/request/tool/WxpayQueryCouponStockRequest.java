package com.stary.pay.wxpay.api.request.tool;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.tool.WxpayQueryCouponStockResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay query_coupon_stock request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpayQueryCouponStockRequest implements WxpayRequest<WxpayQueryCouponStockResponse> {

	/**
	 * 查询代金券批次接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayQueryCouponStockResponse> getResponseClass() {
		return WxpayQueryCouponStockResponse.class;
	}

	@Override
	public boolean isNeedCert() {
		return false;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
