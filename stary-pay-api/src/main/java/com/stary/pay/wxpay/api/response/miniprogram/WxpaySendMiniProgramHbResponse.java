package com.stary.pay.wxpay.api.response.miniprogram;

import com.alibaba.fastjson.annotation.JSONField;
import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay miniprogram sendminiprogramhb response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-29
 * @version 1.3.0
 */
public class WxpaySendMiniProgramHbResponse extends WxpayResponse {

	private static final long serialVersionUID = 4681040660674770274L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;

	/*------------ 以下字段在return_code 、result_code都为SUCCESS时有返回 ------------*/

	/**
	 * 公众账号ID
	 */
	private String wxappid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 商户订单号
	 */
	private String mchBillno;
	/**
	 * 付款金额，单位分
	 */
	private String totalAmount;
	/**
	 * 红包订单的微信单号
	 */
	private String sendListid;
	/**
	 * 用户标识
	 */
	private String reOpenid;

	/**
	 * 返回jaspi的入参package的值
	 */
	@JSONField(name = "package")
	private String packageInfo;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCodeDes() {
		return errCodeDes;
	}

	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}

	public String getWxappid() {
		return wxappid;
	}

	public void setWxappid(String wxappid) {
		this.wxappid = wxappid;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getMchBillno() {
		return mchBillno;
	}

	public void setMchBillno(String mchBillno) {
		this.mchBillno = mchBillno;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSendListid() {
		return sendListid;
	}

	public void setSendListid(String sendListid) {
		this.sendListid = sendListid;
	}

	public String getReOpenid() {
		return reOpenid;
	}

	public void setReOpenid(String reOpenid) {
		this.reOpenid = reOpenid;
	}

	public String getPackageInfo() {
		return packageInfo;
	}

	public void setPackageInfo(String packageInfo) {
		this.packageInfo = packageInfo;
	}


}
