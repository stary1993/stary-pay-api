package com.stary.pay.wxpay.api.request.pay;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.pay.WxpayUnifiedorderResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay unifiedorder request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-11
 */
public class WxpayUnifiedorderRequest implements WxpayRequest<WxpayUnifiedorderResponse> {

	/**
	 * 统一下单接口
	 */
	private String bizContent;

	private String notifyUrl;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public Class<WxpayUnifiedorderResponse> getResponseClass() {
		return WxpayUnifiedorderResponse.class;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	public boolean isNeedEncrypt() {
		return false;
	}

	public void setNeedEncrypt(boolean needEncrypt) {}

	@Override
	public boolean isNeedCert() {
		return false;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API_MCH;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

}
