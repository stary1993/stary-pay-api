package com.stary.pay.wxpay.api.util;

/**
 * <p>请求参数体</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-15
 */
public class RequestParametersHolder {

	/**
	 * 申请参数
	 */
	private WxpayHashMap applicationParams;
		
	public WxpayHashMap getApplicationParams() {
		return applicationParams;
	}
	public void setApplicationParams(WxpayHashMap applicationParams) {
		this.applicationParams = applicationParams;
	}
		
}
