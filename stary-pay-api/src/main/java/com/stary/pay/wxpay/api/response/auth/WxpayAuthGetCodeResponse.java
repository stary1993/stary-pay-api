package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayAuthResponse;

/**
 * <p>wxpayauth get code response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public class WxpayAuthGetCodeResponse extends WxpayAuthResponse {

	private static final long serialVersionUID = 9115421163417979657L;
	
	/**
	 * 获取code的链接
	 */
	private String codeUrl;

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}
		
}
