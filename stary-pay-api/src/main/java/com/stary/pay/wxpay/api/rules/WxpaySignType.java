package com.stary.pay.wxpay.api.rules;
/**
 * <p>签名加密枚举</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-11
 */
public enum WxpaySignType {
        MD5, HMACSHA256
    }