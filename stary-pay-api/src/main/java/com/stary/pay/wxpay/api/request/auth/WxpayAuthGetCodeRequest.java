package com.stary.pay.wxpay.api.request.auth;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayAuthRequest;
import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.response.auth.WxpayAuthGetCodeResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpayauth get code request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public class WxpayAuthGetCodeRequest implements WxpayAuthRequest<WxpayAuthGetCodeResponse> {

	/**
	 * 用户同意授权，获取code接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_OPEN;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayAuthGetCodeResponse> getResponseClass() {
		return WxpayAuthGetCodeResponse.class;
	}

}
