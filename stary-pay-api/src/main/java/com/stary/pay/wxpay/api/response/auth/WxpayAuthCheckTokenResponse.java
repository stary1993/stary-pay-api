package com.stary.pay.wxpay.api.response.auth;

import com.stary.pay.wxpay.api.WxpayAuthResponse;

/**
 * <p>wxpayauth check token resonse</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public class WxpayAuthCheckTokenResponse extends WxpayAuthResponse {

	private static final long serialVersionUID = 9115421163417779657L;
	
}
