package com.stary.pay.wxpay.api.request.auth;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayAuthRequest;
import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.response.auth.WxpayAuthCodeInfoResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpayauth code info request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public class WxpayAuthCodeInfoRequest implements WxpayAuthRequest<WxpayAuthCodeInfoResponse> {

	/**
	 * 通过code换取网页授权access_token接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String serverDomain() {
		return WxpayConstants.DOMAIN_API;
	}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<WxpayAuthCodeInfoResponse> getResponseClass() {
		return WxpayAuthCodeInfoResponse.class;
	}

}
