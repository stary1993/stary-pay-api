package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

/**
 * <p>微信发放小程序红包业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2020-7-29
 * @version 1.3.0
 */
public class WxpaySendMiniProgramHb implements Serializable {

	private static final long serialVersionUID = 7524387142800445300L;

	/**
	 * 商户订单号
	 */
	private String mch_billno;
	/**
	 * 商户名称
	 */
	private String send_name;
	/**
	 * 用户openid
	 */
	private String re_openid;
	/**
	 * 付款金额，单位分
	 */
	private String total_amount;
	/**
	 * 红包发放总人数
	 */
	private String total_num;
	/**
	 * 红包祝福语
	 */
	private String wishing;
	/**
	 * 通知用户形式,通过JSAPI方式领取红包,小程序红包固定传MINI_PROGRAM_JSAPI
	 */
	private String notify_way = "MINI_PROGRAM_JSAPI";
	/**
	 * 活动名称
	 */
	private String act_name;
	/**
	 * 备注信息
	 */
	private String remark;
	/**
	 * 发放红包使用场景，红包金额大于200或者小于1元时必传<br>
	 * PRODUCT_1:商品促销<br>
	 * PRODUCT_2:抽奖<br>
	 * PRODUCT_3:虚拟物品兑奖<br>
	 * PRODUCT_4:企业内部福利<br>
	 * PRODUCT_5:渠道分润<br>
	 * PRODUCT_6:保险回馈<br>
	 * PRODUCT_7:彩票派奖<br>
	 * PRODUCT_8:税务刮奖<br>
	 */
	private String scene_id;

	public WxpaySendMiniProgramHb() {
		super();
	}
	public WxpaySendMiniProgramHb(String mch_billno, String send_name,
			String re_openid, String total_amount, String total_num,
			String wishing, String client_ip, String act_name, String remark) {
		super();
		this.mch_billno = mch_billno;
		this.send_name = send_name;
		this.re_openid = re_openid;
		this.total_amount = total_amount;
		this.total_num = total_num;
		this.wishing = wishing;
		this.act_name = act_name;
		this.remark = remark;
	}

	public WxpaySendMiniProgramHb(String mch_billno, String send_name,
			String re_openid, String total_amount, String total_num,
			String wishing, String client_ip, String act_name, String remark,
			String scene_id) {
		super();
		this.mch_billno = mch_billno;
		this.send_name = send_name;
		this.re_openid = re_openid;
		this.total_amount = total_amount;
		this.total_num = total_num;
		this.wishing = wishing;
		this.act_name = act_name;
		this.remark = remark;
		this.scene_id = scene_id;
	}

	public String getMch_billno() {
		return mch_billno;
	}
	public void setMch_billno(String mch_billno) {
		this.mch_billno = mch_billno;
	}
	public String getSend_name() {
		return send_name;
	}
	public void setSend_name(String send_name) {
		this.send_name = send_name;
	}
	public String getRe_openid() {
		return re_openid;
	}
	public void setRe_openid(String re_openid) {
		this.re_openid = re_openid;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getTotal_num() {
		return total_num;
	}
	public void setTotal_num(String total_num) {
		this.total_num = total_num;
	}
	public String getWishing() {
		return wishing;
	}
	public void setWishing(String wishing) {
		this.wishing = wishing;
	}

	public String getNotify_way() {
		return notify_way;
	}
	public void setNotify_way(String notify_way) {
		this.notify_way = notify_way;
	}
	public String getAct_name() {
		return act_name;
	}
	public void setAct_name(String act_name) {
		this.act_name = act_name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getScene_id() {
		return scene_id;
	}
	public void setScene_id(String scene_id) {
		this.scene_id = scene_id;
	}

}
