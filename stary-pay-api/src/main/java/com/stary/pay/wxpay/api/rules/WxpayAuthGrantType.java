package com.stary.pay.wxpay.api.rules;
/**
 * <p>授权请求 grant_type枚举</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-4
 */
public enum WxpayAuthGrantType {

	authorization_code,
	refresh_token,
	client_credential;



}
