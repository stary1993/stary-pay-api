package com.stary.pay.wxpay.dto.request;

import java.io.Serializable;

/**
 * <p>微信退款查询业务请求参数</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-11
 */
public class WxpayRefundQuery implements Serializable {

	private static final long serialVersionUID = 4735229121198300478L;
	/**
	 * 微信订单号（四选一）
	 */
	private String transaction_id;
	/**
	 * 商户系统内部订单号（四选一）
	 */
	private String out_trade_no;
	/**
	 * 商户系统内部的退款单号（四选一）
	 */
	private String out_refund_no;
	/**
	 * 微信生成的退款单号，在申请退款接口有返回（四选一）
	 */
	private String refund_id;
	/**
	 * 偏移量，当部分退款次数超过10次时可使用，表示返回的查询结果从这个偏移量开始取记录
	 */
	private String offset;
	
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getOut_refund_no() {
		return out_refund_no;
	}
	public void setOut_refund_no(String out_refund_no) {
		this.out_refund_no = out_refund_no;
	}
	public String getRefund_id() {
		return refund_id;
	}
	public void setRefund_id(String refund_id) {
		this.refund_id = refund_id;
	}
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	
}
