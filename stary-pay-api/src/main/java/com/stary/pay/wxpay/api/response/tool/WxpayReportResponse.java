package com.stary.pay.wxpay.api.response.tool;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay report response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-3
 */
public class WxpayReportResponse extends WxpayResponse {

	private static final long serialVersionUID = 942049939513104128L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/
	
	/**
	 * 业务结果，SUCCESS/FAIL
	 */
	private String resultCode;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
			
}
