package com.stary.pay.wxpay.api.request.pay;

import java.util.Map;

import com.stary.pay.wxpay.api.WxpayConstants;
import com.stary.pay.wxpay.api.WxpayRequest;
import com.stary.pay.wxpay.api.response.pay.WxpayRefundNotifyResponse;
import com.stary.pay.wxpay.api.util.WxpayHashMap;

/**
 * <p>wxpay refund notify request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpayRefundNotifyRequest implements WxpayRequest<WxpayRefundNotifyResponse> {
	/**
	 * 退款结果通知数据
	 */
	private String notifyData;

	private boolean isCheckSign = false;

	public String getNotifyData() {
		return notifyData;
	}

	public void setNotifyData(String notifyData) {
		this.notifyData = notifyData;
	}

	@Override
	public boolean isCheckSign() {
		return isCheckSign;
	}

	public void setCheckSign(boolean isCheckSign) {
		this.isCheckSign = isCheckSign;
	}

	@Override
	public String serverDomain() {
		return null;
	}

	@Override
	public boolean isNeedCert() {
		return false;
	}

	@Override
	public String getNotifyUrl() {
		return null;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		WxpayHashMap txtParams = new WxpayHashMap();
		txtParams.put(WxpayConstants.NOTIFY_DATA, this.notifyData);
		return txtParams;
	}

	@Override
	public Class<WxpayRefundNotifyResponse> getResponseClass() {
		return WxpayRefundNotifyResponse.class;
	}

}
