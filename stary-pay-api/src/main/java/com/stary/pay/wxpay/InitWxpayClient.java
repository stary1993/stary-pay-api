package com.stary.pay.wxpay;

import com.stary.pay.wxpay.api.DefaultWxpayClient;
import com.stary.pay.wxpay.api.WxpayApiException;
import com.stary.pay.wxpay.api.WxpayClient;
import com.stary.pay.wxpay.api.util.StringUtils;
import com.stary.pay.wxpay.api.util.WxpayUtils;
import com.stary.pay.wxpay.config.IWxpayConfig;

/**
 * <p>初始化微信请求客户端</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-10
 */
public class InitWxpayClient {

	private static final String DEFAULT_CLIENT_NAME = "WxpayClient";

	/**
	 * 客户端名称
	 */
	private String name = DEFAULT_CLIENT_NAME;

	/**
	 * 微信配置接口
	 */
	private IWxpayConfig wxpayConfig;
	/**
	 * 是否只网页授权
	 */
	private boolean isOnlyAuth;
	/**
	 * 是否服务商
	 */
	private boolean isServiceProvider;

	public InitWxpayClient() {
		super();
	}



	public InitWxpayClient(IWxpayConfig wxpayConfig, boolean isOnlyAuth) {
		super();
		this.wxpayConfig = wxpayConfig;
		this.isOnlyAuth = isOnlyAuth;
	}

	public InitWxpayClient(String name, IWxpayConfig wxpayConfig,
			boolean isOnlyAuth, boolean isServiceProvider) {
		super();
		this.name = name;
		this.wxpayConfig = wxpayConfig;
		this.isOnlyAuth = isOnlyAuth;
		this.isServiceProvider = isServiceProvider;
	}

	public IWxpayConfig getWxpayConfig() {
		return wxpayConfig;
	}

	public void setWxpayConfig(IWxpayConfig wxpayConfig) {
		this.wxpayConfig = wxpayConfig;
	}

	public boolean isOnlyAuth() {
		return isOnlyAuth;
	}

	public void setOnlyAuth(boolean isOnlyAuth) {
		this.isOnlyAuth = isOnlyAuth;
	}

	public boolean isServiceProvider() {
		return isServiceProvider;
	}

	public void setServiceProvider(boolean isServiceProvider) {
		this.isServiceProvider = isServiceProvider;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 创建客户端
	 * @return {@link WxpayClient}
	 * @throws WxpayApiException
	 */
	public WxpayClient build() throws WxpayApiException {
		WxpayClient wxpayClient = null;
		if (this.isOnlyAuth) {
			if (StringUtils.isEmpty(wxpayConfig.appid()) || StringUtils.isEmpty(wxpayConfig.secret())) {
				throw new WxpayApiException("wxpayConfig appid or secret must not null");
			}
			wxpayClient = new DefaultWxpayClient(wxpayConfig.appid(), wxpayConfig.secret())
					.setName(this.name);
			WxpayUtils.getLogger().info("build auth WxpayClient [{}] success.", this.name);
		} else {
			if (StringUtils.isEmpty(wxpayConfig.appid())
					|| StringUtils.isEmpty(wxpayConfig.mchId())
					|| StringUtils.isEmpty(wxpayConfig.key())
					|| StringUtils.isEmpty(wxpayConfig.secret())
					|| StringUtils.isEmpty(wxpayConfig.certFile())) {
				throw new WxpayApiException("wxpayConfig appid or mchId or key or secret or certFile must not null");
			}
			if (this.isServiceProvider) {
				if (StringUtils.isEmpty(wxpayConfig.subAppid())
						|| StringUtils.isEmpty(wxpayConfig.subMchId())
						|| StringUtils.isEmpty(wxpayConfig.subKey())) {
					throw new WxpayApiException("service provider wxpay config subAppid or subMchId or subKey must not null");
				}
				wxpayClient = new DefaultWxpayClient(
						wxpayConfig.appid(), wxpayConfig.subAppid(), wxpayConfig.mchId(), wxpayConfig.subMchId(),
						wxpayConfig.key(), wxpayConfig.subKey(), wxpayConfig.secret(), wxpayConfig.certFile(), wxpayConfig.subCertFile())
						.setName(this.name);
			} else {
				wxpayClient = new DefaultWxpayClient(
						wxpayConfig.appid(), wxpayConfig.mchId(), wxpayConfig.key(), wxpayConfig.secret(), wxpayConfig.certFile())
						.setName(this.name);
			}

			WxpayUtils.getLogger().info("build WxpayClient [{}] success.", this.name);
		}
		return wxpayClient;
	}

}
