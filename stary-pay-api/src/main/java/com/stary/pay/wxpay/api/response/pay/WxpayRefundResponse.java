package com.stary.pay.wxpay.api.response.pay;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay refund response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-15
 */
public class WxpayRefundResponse extends WxpayResponse {

	private static final long serialVersionUID = 5822703842121393259L;
	
	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 子商户公众账号ID
	 */
	private String subAppid;
	/**
	 * 子商户号
	 */
	private String subMchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;	
	/**
	 * 微信订单号
	 */
	private String transactionId;
	/**
	 * 商户订单号
	 */
	private String outTradeNo;
	/**
	 * 商户退款单号
	 */
	private String outRefundNo;
	/**
	 * 微信退款单号
	 */
	private String refundId;
	/**
	 * 退款总金额,单位为分
	 */
	private String refundFee;
	/**
	 * 订单总金额，单位为分
	 */
	private String totalFee;
	/**
	 * 现金支付金额，单位为分
	 */
	private String cashFee;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getSubAppid() {
		return subAppid;
	}

	public void setSubAppid(String subAppid) {
		this.subAppid = subAppid;
	}

	public String getSubMchId() {
		return subMchId;
	}

	public void setSubMchId(String subMchId) {
		this.subMchId = subMchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getOutTradeNo() {
		return outTradeNo;
	}
	
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	
	public String getOutRefundNo() {
		return outRefundNo;
	}
	
	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}
	
	public String getRefundId() {
		return refundId;
	}
	
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	
	public String getRefundFee() {
		return refundFee;
	}
	
	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}
	
	public String getTotalFee() {
		return totalFee;
	}
	
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	
	public String getCashFee() {
		return cashFee;
	}
	
	public void setCashFee(String cashFee) {
		this.cashFee = cashFee;
	}
	
}
