package com.stary.pay.wxpay.api.response.tool;

import com.stary.pay.wxpay.api.WxpayResponse;

/**
 * <p>wxpay send_coupon response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-5-31
 */
public class WxpaySendCouponResponse extends WxpayResponse {

	private static final long serialVersionUID = 7506508026795646550L;

	/*------------ 以下字段在return_code为SUCCESS的时候有返回  ------------*/

	/**
	 * 公众账号ID
	 */
	private String appid;
	/**
	 * 商户号
	 */
	private String mchId;
	/**
	 * 随机字符串
	 */
	private String nonceStr;
	/**
	 * 微信支付分配的终端设备号
	 */
	private String deviceInfo;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 业务结果 SUCCESS/FAIL
	 */
	private String resultCode;
	/**
	 * 错误代码 当result_code为FAIL时返回
	 */
	private String errCode;
	/**
	 * 错误代码描述 当result_code为FAIL时返回
	 */
	private String errCodeDes;
	/**
	 * 代金券批次id
	 */
	private String couponStockId;
	/**
	 * 返回记录数
	 */
	private String respCount;
	/**
	 * 成功记录数
	 */
	private String successCount;
	/**
	 * 失败记录数
	 */
	private String failedCount;
	/**
	 * 用户在商户appid下的唯一标识
	 */
	private String openid;
	/**
	 * 对一个用户成功发放代金券则返回代金券id，即ret_code为SUCCESS的时候
	 */
	private String couponId;
	/**
	 * 返回码，SUCCESS/FAILED
	 */
	private String retCode;
	/**
	 * 返回信息，当返回码是FAILED的时候填写，否则填空串“”
	 */
	private String retMsg;
	
	public String getAppid() {
		return appid;
	}
	
	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	public String getMchId() {
		return mchId;
	}
	
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	
	public String getNonceStr() {
		return nonceStr;
	}
	
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	
	public String getDeviceInfo() {
		return deviceInfo;
	}
	
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	public String getSign() {
		return sign;
	}
	
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	
	public String getErrCodeDes() {
		return errCodeDes;
	}
	
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}
	
	public String getCouponStockId() {
		return couponStockId;
	}
	
	public void setCouponStockId(String couponStockId) {
		this.couponStockId = couponStockId;
	}
	
	public String getRespCount() {
		return respCount;
	}
	
	public void setRespCount(String respCount) {
		this.respCount = respCount;
	}
	
	public String getSuccessCount() {
		return successCount;
	}
	
	public void setSuccessCount(String successCount) {
		this.successCount = successCount;
	}
	
	public String getFailedCount() {
		return failedCount;
	}
	
	public void setFailedCount(String failedCount) {
		this.failedCount = failedCount;
	}
	
	public String getOpenid() {
		return openid;
	}
	
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getCouponId() {
		return couponId;
	}
	
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	
	public String getRetCode() {
		return retCode;
	}
	
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	
	public String getRetMsg() {
		return retMsg;
	}
	
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}	
	
}
