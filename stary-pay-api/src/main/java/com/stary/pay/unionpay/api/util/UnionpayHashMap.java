package com.stary.pay.unionpay.api.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.stary.pay.unionpay.api.UnionpayConstants;


/**
 * <p>纯字符串字典结构</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-20
 */
public class UnionpayHashMap extends HashMap<String, String> {

	private static final long serialVersionUID = 1277791390393392630L;

	public UnionpayHashMap() {
		super();
	}

	public UnionpayHashMap(Map<? extends String, ? extends String> m) {
		super(m);
	}

	public String put(String key, Object value) {
		String strValue;

		if (value == null) {
			strValue = null;
		} else if (value instanceof String) {
			strValue = (String) value;
		} else if (value instanceof Integer) {
			strValue = ((Integer) value).toString();
		} else if (value instanceof Long) {
			strValue = ((Long) value).toString();
		} else if (value instanceof Float) {
			strValue = ((Float) value).toString();
		} else if (value instanceof Double) {
			strValue = ((Double) value).toString();
		} else if (value instanceof Boolean) {
			strValue = ((Boolean) value).toString();
		} else if (value instanceof Date) {
            DateFormat format = new SimpleDateFormat(UnionpayConstants.DATE_TIME_FORMAT);
            format.setTimeZone(TimeZone.getTimeZone(UnionpayConstants.DATE_TIMEZONE));
			strValue = format.format((Date) value);
		} else {
			strValue = value.toString();
		}
		return this.put(key, strValue);
	}

	@Override
	public String put(String key, String value) {
		if (StringUtils.areNotEmpty(key, value)) {
			return super.put(key, value);
		} else {
			return null;
		}
	}

}
