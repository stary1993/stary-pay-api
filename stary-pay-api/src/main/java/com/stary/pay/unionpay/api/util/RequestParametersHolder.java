package com.stary.pay.unionpay.api.util;

/**
 * <p>请求参数体</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-24
 */
public class RequestParametersHolder {

	/**
	 * 申请参数
	 */
	private UnionpayHashMap applicationParams;
		
	public UnionpayHashMap getApplicationParams() {
		return applicationParams;
	}
	public void setApplicationParams(UnionpayHashMap applicationParams) {
		this.applicationParams = applicationParams;
	}
	
}
