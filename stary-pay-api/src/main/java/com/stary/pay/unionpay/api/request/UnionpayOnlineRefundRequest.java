package com.stary.pay.unionpay.api.request;

import java.util.Map;

import com.stary.pay.unionpay.api.UnionpayConstants;
import com.stary.pay.unionpay.api.UnionpayRequest;
import com.stary.pay.unionpay.api.response.UnionpayOnlineRefundResponse;
import com.stary.pay.unionpay.api.util.UnionpayHashMap;

/**
 * <p>unionpay online refund request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-2
 */
public class UnionpayOnlineRefundRequest implements UnionpayRequest<UnionpayOnlineRefundResponse> {
	/**
	 * 在线网关支付退货交易接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	private String backUrl;


	@Override
	public String getFrontUrl() {
		return null;
	}

	@Override
	public void setFrontUrl(String frontUrl) {}

	@Override
	public String getBackUrl() {
		return backUrl;
	}

	@Override
	public void setBackUrl(String backUrl) {
		this.backUrl = backUrl;
	}

	@Override
	public Map<String, String> getTextParams() {
		UnionpayHashMap txtParams = new UnionpayHashMap();
		txtParams.put(UnionpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<UnionpayOnlineRefundResponse> getResponseClass() {
		return UnionpayOnlineRefundResponse.class;
	}

	@Override
	public boolean isCheckSign() {
		return true;
	}

	@Override
	public String getTxnType() {
		return UnionpayConstants.TXNTYPE_ONLINE_REFUND;
	}

	@Override
	public String getTxnSubType() {
		return UnionpayConstants.TXNSUBTYPE_01;
	}

	@Override
	public String getBizType() {
		return UnionpayConstants.BIZTYPE_ONLINE;
	}

	@Override
	public String getChannelType() {
		return UnionpayConstants.CHANNELTYPE_PAGE;
	}
}
