package com.stary.pay.unionpay.api;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>unionpay基本响应信息</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-20
 */
public class UnionpayResponse implements Serializable {

	private static final long serialVersionUID = 5689213904867659656L;
	/**
	 * 应答码，同步应答00，仅表示银联受理成功，不表示订单成功。
	 */
	private String respCode;
	/**
	 * 应答说明
	 */
	private String respMsg;
	/**
	 * html体
	 */
	private String body;
	/**
	 * 请求参数
	 */
    private Map<String, String> params;
	/**
	 * 版本号
	 */
	private String version;
	/**
	 * 编码方式
	 */
	private String encoding;
	
    public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
	public boolean isSuccess() {
		return "00".equals(this.respCode);
	}
		
}
