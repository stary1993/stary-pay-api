package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay online page pay response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-25
 */
public class UnionpayOnlinePagePayResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5276545127744200045L;
	
}
