package com.stary.pay.unionpay.config;

import com.stary.pay.unionpay.api.rules.UnionpayAccessType;


/**
 * <p>银联配置信息接口</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-5
 */
public interface IUnionpayConfig {

	/**
	 * 商户号
	 */
	public String merId();
	/**
	 * 接入类型
	 */
	public UnionpayAccessType accessType();
	/**
	 * 安全密钥(SHA256和SM3计算时使用)
	 */
	public String secureKey();
	/**
	 * 中级证书路径
	 */
	public String middleCertFile();
	/**
	 * 根证书路径
	 */
	public String rootCertFile();
	/**
	 * 签名证书路径
	 */
	public String signCertFile();
	/**
	 * 签名证书密码
	 */
	public String signCertPwd();
	/**
	 * 加密公钥证书路径
	 */
	public String encryptCertFile();
	/**
	 * 是否测试
	 * @return
	 */
	public boolean isTest();

}
