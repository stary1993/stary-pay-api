package com.stary.pay.unionpay.api.request;

import java.util.Map;

import com.stary.pay.unionpay.api.UnionpayConstants;
import com.stary.pay.unionpay.api.UnionpayRequest;
import com.stary.pay.unionpay.api.response.UnionpayEncryptCertUpdateQueryResponse;
import com.stary.pay.unionpay.api.util.UnionpayHashMap;

/**
 * <p>unionpay encrypt cert update query request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-9
 */
public class UnionpayEncryptCertUpdateQueryRequest implements UnionpayRequest<UnionpayEncryptCertUpdateQueryResponse> {
	/**
	 * 银联加密公钥更新查询接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getFrontUrl() {
		return null;
	}

	@Override
	public void setFrontUrl(String frontUrl) {}

	@Override
	public String getBackUrl() {
		return null;
	}

	@Override
	public void setBackUrl(String backUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		UnionpayHashMap txtParams = new UnionpayHashMap();
		txtParams.put(UnionpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<UnionpayEncryptCertUpdateQueryResponse> getResponseClass() {
		return UnionpayEncryptCertUpdateQueryResponse.class;
	}

	@Override
	public boolean isCheckSign() {
		return true;
	}

	@Override
	public String getTxnType() {
		return UnionpayConstants.TXNTYPE_ENCRYPT_CERT_UPDATE_QUERY;
	}

	@Override
	public String getTxnSubType() {
		return UnionpayConstants.TXNSUBTYPE_00;
	}

	@Override
	public String getBizType() {
		return UnionpayConstants.BIZTYPE_DEFAULT;
	}

	@Override
	public String getChannelType() {
		return UnionpayConstants.CHANNELTYPE_PAGE;
	}

}
