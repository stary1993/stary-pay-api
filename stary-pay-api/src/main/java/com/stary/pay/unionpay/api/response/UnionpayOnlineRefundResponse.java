package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay online refund response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-2
 */
public class UnionpayOnlineRefundResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5271545127744201041L;

	/**
	 * 原始交易流水号，原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
	 */
	private String origQryId;
	/**
	 * 被查询交易查询流水号
	 */
	private String queryId;
	/**
	 * 交易类型
	 */
	private String txnType;
	/**
	 * 交易子类
	 */
	private String txnSubType;
	/**
	 * 签名
	 */
	private String signature;
	/**
	 * 签名方法
	 */
	private String signMethod;
	/**
	 * 交易金额，分
	 */
	private String txnAmt;
	/**
	 * 签名公钥证书，用RSA签名方式时必选，此域填写银联签名公钥证书。
	 */
	private String signPubKeyCert;
	/**
	 * 接入类型 0：商户直连接入1：收单机构接入2：平台商户接入
	 */
	private String accessType;
	/**
	 * 产品类型
	 */
	private String bizType;
	/**
	 * 编码方式
	 */
	private String encoding;
	/**
	 * 商户代码
	 */
	private String merId;
	/**
	 * 订单发送时间，YYYYMMDDhhmmss
	 */
	private String txnTime;
	/**
	 * 商户订单号
	 */
	private String orderId;
	/**
	 * 版本号
	 */
	private String version;

	public String getOrigQryId() {
		return origQryId;
	}

	public void setOrigQryId(String origQryId) {
		this.origQryId = origQryId;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getTxnSubType() {
		return txnSubType;
	}

	public void setTxnSubType(String txnSubType) {
		this.txnSubType = txnSubType;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSignMethod() {
		return signMethod;
	}

	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}

	public String getTxnAmt() {
		return txnAmt;
	}

	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}

	public String getSignPubKeyCert() {
		return signPubKeyCert;
	}

	public void setSignPubKeyCert(String signPubKeyCert) {
		this.signPubKeyCert = signPubKeyCert;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	@Override
	public String getEncoding() {
		return encoding;
	}

	@Override
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getMerId() {
		return merId;
	}

	public void setMerId(String merId) {
		this.merId = merId;
	}

	public String getTxnTime() {
		return txnTime;
	}

	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public void setVersion(String version) {
		this.version = version;
	}

}
