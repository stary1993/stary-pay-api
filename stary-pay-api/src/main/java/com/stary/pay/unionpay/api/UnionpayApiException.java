package com.stary.pay.unionpay.api;

/**
 * <p>银联业务异常类</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-20
 */
public class UnionpayApiException extends Exception {

	private static final long serialVersionUID = 8418749209383288035L;

	public UnionpayApiException() {
		super();
	}

	public UnionpayApiException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnionpayApiException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnionpayApiException(String message) {
		super(message);
	}

	public UnionpayApiException(Throwable cause) {
		super(cause);
	}		

}
