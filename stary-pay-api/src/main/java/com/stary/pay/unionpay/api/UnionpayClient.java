package com.stary.pay.unionpay.api;

/**
 * <p>unionpay client</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-21
 * @version 1.3.0
 */
public interface UnionpayClient {

	/**
	 * 客户端名称
	 * @return
	 */
	public String getName();

	/**
	 * 初始化加载证书
	 */
	public void initCerts();
	/**
	 * 请求
	 * @param <T>
	 * @param request
	 * @return
	 * @throws UnionpayApiException
	 */
	public <T extends UnionpayResponse> T execute(UnionpayRequest<T> request) throws UnionpayApiException;
	/**
	 * 页面请求
	 * @param <T>
	 * @param request
	 * @return
	 * @throws UnionpayApiException
	 */
	public <T extends UnionpayResponse> T pageExecute(UnionpayRequest<T> request) throws UnionpayApiException;
}
