package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay file download bill response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-2
 */
public class UnionpayFileDownloadBillResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5271545127704201041L;	
	/**
	 * 交易类型
	 */
	private String txnType;
	/**
	 * 交易子类
	 */
	private String txnSubType;
	/**
	 * 发送时间，YYYYMMDDhhmmss
	 */
	private String txnTime;
	/**
	 * 签名
	 */
	private String signature;
	/**
	 * 签名方法
	 */
	private String signMethod;
	/**
	 * 清算日期
	 */
	private String settleDate;
	/**
	 * 签名公钥证书，用RSA签名方式时必选，此域填写银联签名公钥证书。
	 */
	private String signPubKeyCert;
	/**
	 * 接入类型 0：商户直连接入1：收单机构接入2：平台商户接入
	 */
	private String accessType;
	/**
	 * 产品类型
	 */
	private String bizType;
	/**
	 * 商户代码
	 */
	private String merId;
	/**
	 * 文件类型
	 */
	private String fileType;
	/**
	 * 文件内容
	 */
	private String fileContent;
	/**
	 * 文件名
	 */
	private String fileName;
	
	public String getTxnType() {
		return txnType;
	}
	
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	
	public String getTxnSubType() {
		return txnSubType;
	}
	
	public void setTxnSubType(String txnSubType) {
		this.txnSubType = txnSubType;
	}
	
	public String getTxnTime() {
		return txnTime;
	}
	
	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public String getSignMethod() {
		return signMethod;
	}
	
	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}
	
	public String getSettleDate() {
		return settleDate;
	}
	
	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
	public String getSignPubKeyCert() {
		return signPubKeyCert;
	}
	
	public void setSignPubKeyCert(String signPubKeyCert) {
		this.signPubKeyCert = signPubKeyCert;
	}
	
	public String getAccessType() {
		return accessType;
	}
	
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	
	public String getBizType() {
		return bizType;
	}
	
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
		
	public String getMerId() {
		return merId;
	}
	
	public void setMerId(String merId) {
		this.merId = merId;
	}
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public String getFileContent() {
		return fileContent;
	}
	
	
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
