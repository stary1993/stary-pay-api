package com.stary.pay.unionpay.api;

import org.apache.http.client.HttpClient;

/**
 * <p>银联基础常量</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-21
 */
public class UnionpayConstants {

	public static final String USER_AGENT								=
			" (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
			") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();

	public final static String COLUMN_DEFAULT							= "-";

	public final static String KEY_DELIMITER				  			= "#";
	/** memeber variable: blank. */
	public static final String BLANK						  			= "";
	/** member variabel: space. */
	public static final String SPACE						  			= " ";
	/** memeber variable: unline. */
	public static final String UNLINE						 	 		= "_";
	/** memeber varibale: star. */
	public static final String STAR							  			= "*";
	/** memeber variable: line. */
	public static final String LINE					  		  			= "-";
	/** memeber variable: add. */
	public static final String ADD							  			= "+";
	/** memeber variable: colon. */
	public final static String COLON						  			= "|";
	/** memeber variable: point. */
	public final static String POINT						  			= ".";
	/** memeber variable: comma. */
	public final static String COMMA						  			= ",";
	/** memeber variable: slash. */
	public final static String SLASH						  			= "/";
	/** memeber variable: div. */
	public final static String DIV 							  			= "/";
	/** memeber variable: left . */
	public final static String LB 							  			= "(";
	/** memeber variable: right. */
	public final static String RB							  			= ")";
	/** memeber variable: rmb. */
	public final static String CUR_RMB						  			= "RMB";
	/** memeber variable: .page size */
	public static final int PAGE_SIZE						 		 	= 10;
	/** memeber variable: String ONE. */
	public static final String ONE							  			= "1";
	/** memeber variable: String ZERO. */
	public static final String ZERO							  			= "0";
	/** memeber variable: number six. */
	public static final int NUM_SIX							  			= 6;
	/** memeber variable: equal mark. */
	public static final String EQUAL 						  			= "=";
	/** memeber variable: operation ne. */
	public static final String NE							  			= "!=";
	/** memeber variable: operation le. */
	public static final String LE							  			= "<=";
	/** memeber variable: operation ge. */
	public static final String GE							  			= ">=";
	/** memeber variable: operation lt. */
	public static final String LT							  			= "<";
	/** memeber variable: operation gt. */
	public static final String GT							  			= ">";
	/** memeber variable: list separator. */
	public static final String SEP							  			= "./";
	/** memeber variable: Y. */
	public static final String Y							  			= "Y";
	/** memeber variable: AMPERSAND. */
	public static final String AMPERSAND					  			= "&";
	/** memeber variable: SQL_LIKE_TAG. */
	public static final String SQL_LIKE_TAG					  			= "%";
	/** memeber variable: @. */
	public static final String MAIL							  			= "@";
	/** memeber variable: number zero. */
	public static final int NZERO							  			= 0;

	public static final String LEFT_BRACE					  			= "{";

	public static final String RIGHT_BRACE					  			= "}";
	/** memeber variable: string true. */
	public static final String TRUE_STRING					  			= "true";
	/** memeber variable: string false. */
	public static final String FALSE_STRING					  			= "false";
	/** memeber variable: forward success. */
	public static final String SUCCESS						  			= "success";
	/** memeber variable: forward fail. */
	public static final String FAIL							  			= "fail";
	/** memeber variable: global forward success. */
	public static final String GLOBAL_SUCCESS				  			= "$success";
	/** memeber variable: global forward fail. */
	public static final String GLOBAL_FAIL					  			= "$fail";

	public static final String FILE_SUFFIX_TXT                			= ".txt";

	public static final String VERSION              	 	  			= "version";

	public static final String ENCODING              	 	  			= "encoding";
	/** 商户代码. */
	public static final String MERID              	 	  	  			= "merId";

	public static final String ACCESSTYPE 					 			= "accessType";
	/** 签名. */
	public static final String SIGNATURE 					  			= "signature";
	/** 签名方法. */
	public static final String SIGNMETHOD					  			= "signMethod";
	/** 交易类型. */
	public static final String TXNTYPE 						  			= "txnType";
	/** 前台通知地址 . */
	public static final String FRONTURL						  			= "frontUrl";

	public static final String FRONTFIALURL					  			= "frontFailUrl";
	/** 后台通知地址. */
	public static final String BACKURL						  			= "backUrl";
	/** 交易时间. */
	public static final String TXNTIME		 				  			= "txnTime";

	public static final String TXNAMT		 				  			= "txnAmt";

	public static final String RISKRATEINFO		 				  		= "riskRateInfo";

	public static final String TXNTYPE_ONLINE_QUERY 		  			= "00";

	public static final String TXNTYPE_ONLINE_PAGE_PAY 		  			= "01";

	public static final String TXNTYPE_ONLINE_PAGE_PREAUTH_PAY			= "02";

	public static final String TXNTYPE_ONLINE_PREAUTH_FINISH  			= "03";

	public static final String TXNTYPE_ONLINE_REFUND 		  			= "04";

	public static final String TXNTYPE_ONLINE_PREAUTH_UNDO 	 		 	= "31";

	public static final String TXNTYPE_ONLINE_UNDO 			  			= "32";

	public static final String TXNTYPE_ONLINE_PREAUTH_FINISH_UNDO		= "33";

	public static final String TXNTYPE_FILE_BILL			  			= "76";

	public static final String TXNTYPE_ENCRYPT_CERT_UPDATE_QUERY		= "95";

	public static final String VERSION_1_0_0 				  			= "1.0.0";

	public static final String VERSION_5_0_0				  			= "5.0.0";

	public static final String VERSION_5_0_1				  			= "5.0.1";

	public static final String VERSION_5_1_0				  			= "5.1.0";

	public static final String VERSION_6_0_0				  			= "6.0.0";

	public static final String SIGNMETHOD_RSA				  			= "01";

	public static final String SIGNMETHOD_SHA256			  			= "11";

	public static final String SIGNMETHOD_SM3 				  			= "12";

	public static final String UNIONPAY_CNNAME				  			= "中国银联股份有限公司";
	/** 敏感信息加密公钥 */
	public static final String CERT_TYPE_01 				  			= "01";
	/** 磁道加密公钥 */
	public static final String CERT_TYPE_02 				  			= "02";

	public static final String SIGN_CERT_TYPE_PKCS12          			= "PKCS12";

	public static final String DATE_TIME_FORMAT               			= "yyyyMMddHHmmss";

	public static final String DATE_TIMEZONE                 		 	= "GMT+8";

	public static final String FORMAT_XML                     			= "xml";

	public static final String FORMAT_JSON                    			= "json";

	public static final String CHARSET_UTF8                   			= "UTF-8";

	public static final String HTTPS                    	  			= "https://";

	public static final String HTTP                    		  			= "http://";

	public static final String BIZ_CONTENT                    			= "biz_content";
	/** 证书ID. */
	public static final String CERTID 						  			= "certId";

	public static final String SETTLEDATE						  		= "settleDate";
	/** 文件类型. */
	public static final String FILETYPE						  			= "fileType";
	/** 文件名称. */
	public static final String FILENAME						  			= "fileName";
	/** 批量文件内容. */
	public static final String FILECONTENT					  			= "fileContent";
	/** 交易子类. */
	public static final String TXNSUBTYPE				 	  			= "txnSubType";
	/** 交易子类，预授权 */
	public static final String TXNSUBTYPE_00				 	  		= "00";
	/** 交易子类，自助消费，预授权 */
	public static final String TXNSUBTYPE_01				 	  		= "01";
	/** 交易子类，分期付款  */
	public static final String TXNSUBTYPE_03				 	  		= "03";
	/** 业务类型. */
	public static final String BIZTYPE						  			= "bizType";
	/** 业务类型，默认 */
	public static final String BIZTYPE_DEFAULT						  	= "000000";
	/** 业务类型，B2C网关支付，手机wap支付 */
	public static final String BIZTYPE_ONLINE				  			= "000201";
	/** 业务类型 认证支付2.0 */
	public static final String BIZTYPE_NOJUMP				  			= "000301";
	/** 渠道类型 */
	public static final String CHANNELTYPE				  				= "channelType";
	/** 渠道类型，B2C网关支付和手机wap支付；PC，平板 */
	public static final String CHANNELTYPE_PAGE			  				= "07";
	/** 签名公钥证书 */
	public static final String SIGNPUBKEYCERT							= "signPubKeyCert";
	/** 加密公钥证书 */
	public static final String ENCRYPTPUBKEYCERT						= "encryptPubKeyCert";
	/** 证书类型 */
	public static final String CERTTYPE 								= "certType";
	/** 币种. */
	public static final String CURRENCYCODE 							= "currencyCode";

	public static final String CURRENCYCODE_RMB 						= "156";

	public static final String PAYTIMEOUT 								= "payTimeout";
	/** 批次号. */
	public static final String BATCHNO 						  			= "batchNo";

	public static final String ORDERID 						  			= "orderId";

	public static final String ORIGQRYID 						  		= "origQryId";


	/*--------------------------------------------------------------------------------------------------
	-------------------------------------- 网银业务 API --------------------------------------
--------------------------------------------------------------------------------------------------*/
	public static final String DOMAIN_GATEWAY_TEST 		      			= HTTPS + "gateway.test.95516.com";
	public static final String DOMAIN_GATEWAY				  			= HTTPS + "gateway.95516.com";

	public static final String DOMAIN_FILEDOWLOAD_TEST		  			= HTTPS + "filedownload.test.95516.com/";
	public static final String DOMAIN_FILEDOWLOAD			  			= HTTPS + "filedownload.95516.com/";

	public static final String FRONTTRANS_URL_SUFFIX 		  			= "/gateway/api/frontTransReq.do";
	public static final String BACKTRANS_URL_SUFFIX			  			= "/gateway/api/backTransReq.do";
	public static final String QUERYTRANS_URL_SUFFIX		  			= "/gateway/api/queryTrans.do";
	public static final String BATCHTRANS_URL_SUFFIX	      			= "/gateway/api/batchTrans.do";
	public static final String APPTRANS_URL_SUFFIX			  			= "/gateway/api/appTransReq.do";
	public static final String CARDTRANS_URL_SUFFIX			  			= "/gateway/api/cardTransReq.do";

	/*以下缴费产品使用，其余产品用不到*/
	public static final String JFFRONTTRANS_URL_SUFFIX		  			= "/jiaofei/api/frontTransReq.do";
	public static final String JFBACKTRANS_URL_SUFFIX		  			= "/jiaofei/api/backTransReq.do";
	public static final String JFQUERYTRANS_URL_SUFFIX		  			= "/jiaofei/api/queryTrans.do";
	public static final String JFCARDTRANS_URL_SUFFIX		  			= "/jiaofei/api/cardTransReq.do";
	public static final String JFAPPTRANS_URL_SUFFIX		  			= "/jiaofei/api/appTransReq.do";

}
