package com.stary.pay.unionpay.api.rules;
/**
 * <p>接入类型枚举</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-21
 */
public enum UnionpayAccessType {
	/**
	 * 商户直连接入
	 */
	MCH_DIRECT_CONNECT("0", "商户直连接入"),
	/**
	 * 收单机构接入
	 */
	ACQUIRER("1", "收单机构接入"),
	/**
	 * 平台商户接入
	 */
	PLATFORM_MCH("2", "平台商户接入");
	/**
	 * 类型
	 */
	private String type;
	/**
	 * 说明
	 */
	private String desc;
	
	private UnionpayAccessType(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
