package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay online query response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-1
 */
public class UnionpayOnlineQueryResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5276545127744201045L;	
	/**
	 * 被查询交易查询流水号
	 */
	private String queryId;
	/**
	 * 交易传输时间，MMDDhhmmss
	 */
	private String traceTime;
	/**
	 * 交易类型
	 */
	private String txnType;
	/**
	 * 交易子类
	 */
	private String txnSubType;
	/**
	 * 签名
	 */
	private String signature;
	/**
	 * 签名方法
	 */
	private String signMethod;
	/**
	 * 清算币种
	 */
	private String settleCurrencyCode;
	/**
	 * 清算金额
	 */
	private String settleAmt;
	/**
	 * 清算日期，MMDD
	 */
	private String settleDate;
	/**
	 * 系统跟踪号
	 */
	private String traceNo;
	/**
	 * 交易币种
	 */
	private String currencyCode;
	/**
	 * 交易金额，分
	 */
	private String txnAmt;
	/**
	 * 签名公钥证书，用RSA签名方式时必选，此域填写银联签名公钥证书。
	 */
	private String signPubKeyCert;
	/**
	 * 原交易应答码
	 */
	private String origRespCode;
	/**
	 * 原交易应答信息
	 */
	private String origRespMsg;
	/**
	 * 账号，根据商户配置返回 
	 */
	private String accNo;
	/**
	 * 支付方式
	 */
	private String payType;
	/**
	 * 支付卡标识，（移动支付消费）根据商户配置返回
	 */
	private String payCardNo;
	/**
	 * 支付卡类型
	 */
	private String payCardType;
	/**
	 * 接入类型 0：商户直连接入1：收单机构接入2：平台商户接入
	 */
	private String accessType;
	/**
	 * 产品类型
	 */
	private String bizType;
	/**
	 * 商户代码
	 */
	private String merId;
	/**
	 * 订单发送时间，YYYYMMDDhhmmss
	 */
	private String txnTime;
	/**
	 * 商户订单号
	 */
	private String orderId;
	
	public String getQueryId() {
		return queryId;
	}
	
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	
	public String getTraceTime() {
		return traceTime;
	}
	
	public void setTraceTime(String traceTime) {
		this.traceTime = traceTime;
	}
	
	public String getTxnType() {
		return txnType;
	}
	
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	
	public String getTxnSubType() {
		return txnSubType;
	}
	
	public void setTxnSubType(String txnSubType) {
		this.txnSubType = txnSubType;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getSignMethod() {
		return signMethod;
	}
	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}
	
	public String getSettleCurrencyCode() {
		return settleCurrencyCode;
	}
	
	public void setSettleCurrencyCode(String settleCurrencyCode) {
		this.settleCurrencyCode = settleCurrencyCode;
	}
	
	public String getSettleAmt() {
		return settleAmt;
	}
	
	public void setSettleAmt(String settleAmt) {
		this.settleAmt = settleAmt;
	}
	
	public String getSettleDate() {
		return settleDate;
	}
	
	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
	public String getTraceNo() {
		return traceNo;
	}
	
	public void setTraceNo(String traceNo) {
		this.traceNo = traceNo;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public String getTxnAmt() {
		return txnAmt;
	}
	
	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}
	
	public String getSignPubKeyCert() {
		return signPubKeyCert;
	}
	
	public void setSignPubKeyCert(String signPubKeyCert) {
		this.signPubKeyCert = signPubKeyCert;
	}
	
	public String getOrigRespCode() {
		return origRespCode;
	}
	
	public void setOrigRespCode(String origRespCode) {
		this.origRespCode = origRespCode;
	}
	
	public String getOrigRespMsg() {
		return origRespMsg;
	}
	
	public void setOrigRespMsg(String origRespMsg) {
		this.origRespMsg = origRespMsg;
	}
	
	public String getAccNo() {
		return accNo;
	}
	
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	
	public String getPayType() {
		return payType;
	}
	
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	public String getPayCardNo() {
		return payCardNo;
	}
	
	public void setPayCardNo(String payCardNo) {
		this.payCardNo = payCardNo;
	}
	
	public String getPayCardType() {
		return payCardType;
	}
	
	public void setPayCardType(String payCardType) {
		this.payCardType = payCardType;
	}
	
	public String getAccessType() {
		return accessType;
	}
	
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	
	public String getBizType() {
		return bizType;
	}
	
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getMerId() {
		return merId;
	}
	
	public void setMerId(String merId) {
		this.merId = merId;
	}
	
	public String getTxnTime() {
		return txnTime;
	}
	
	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
