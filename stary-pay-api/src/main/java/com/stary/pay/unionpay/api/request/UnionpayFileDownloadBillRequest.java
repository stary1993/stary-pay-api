package com.stary.pay.unionpay.api.request;

import java.util.Map;

import com.stary.pay.unionpay.api.UnionpayConstants;
import com.stary.pay.unionpay.api.UnionpayRequest;
import com.stary.pay.unionpay.api.response.UnionpayFileDownloadBillResponse;
import com.stary.pay.unionpay.api.util.UnionpayHashMap;

/**
 * <p>unionpay file download bill request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-2
 */
public class UnionpayFileDownloadBillRequest implements UnionpayRequest<UnionpayFileDownloadBillResponse> {
	/**
	 * 对账文件下载接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getFrontUrl() {
		return null;
	}

	@Override
	public void setFrontUrl(String frontUrl) {}

	@Override
	public String getBackUrl() {
		return null;
	}

	@Override
	public void setBackUrl(String backUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		UnionpayHashMap txtParams = new UnionpayHashMap();
		txtParams.put(UnionpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<UnionpayFileDownloadBillResponse> getResponseClass() {
		return UnionpayFileDownloadBillResponse.class;
	}

	@Override
	public boolean isCheckSign() {
		return true;
	}

	@Override
	public String getTxnType() {
		return UnionpayConstants.TXNTYPE_FILE_BILL;
	}

	@Override
	public String getTxnSubType() {
		return UnionpayConstants.TXNSUBTYPE_01;
	}

	@Override
	public String getBizType() {
		return UnionpayConstants.BIZTYPE_DEFAULT;
	}

	@Override
	public String getChannelType() {
		return null;
	}

}
