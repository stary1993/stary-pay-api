package com.stary.pay.unionpay.api;

import java.util.Map;

/**
 * <p>支付请求接口</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-56-21
 */
public interface UnionpayRequest <T extends UnionpayResponse> {
	
	/**
	 * 获取前台通知地址
	 * @return 前台通知地址
	 */
	public String getFrontUrl();
	/**
	 * 设置前台通知地址
	 * @param frontUrl 前台通知地址
	 */
	public void setFrontUrl(String frontUrl);
	/**
	 * 获取后台通知地址
	 * @return 后台通知地址
	 */
	public String getBackUrl();
	/**
	 * 设置后台通知地址
	 * @param backUrl 后台通知地址
	 */
	public void setBackUrl(String backUrl);		
	/**
	 * 是否验证签名
	 * @return true/false
	 */
	public boolean isCheckSign();
	/**
	 * 获取交易类型
	 * @return 交易类型
	 */
	public String getTxnType();
	/**
	 * 获取交易子类
	 * @return 交易子类
	 */
	public String getTxnSubType();	
	/**
	 * 获取业务类型
	 * @return 业务类型
	 */
	public String getBizType();
	/**
	 * 获取渠道类型
	 * @return 渠道类型
	 */
	public String getChannelType();
	/**
	 * 获取所有的Key-Value形式的文本请求参数集合。其中：
	 * <ul>
	 * <li>Key: 请求参数名</li>
	 * <li>Value: 请求参数值</li>
	 * </ul>
	 * @return 文本请求参数集合
	 */
	public Map<String, String> getTextParams();
	/**
	 * 得到当前API的响应结果类型
	 * @return 响应类型
	 */
	public Class<T> getResponseClass();
	
}
