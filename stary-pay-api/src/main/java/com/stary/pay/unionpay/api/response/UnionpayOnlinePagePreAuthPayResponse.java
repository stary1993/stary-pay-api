package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay online page pre-auth pay response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-3
 */
public class UnionpayOnlinePagePreAuthPayResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5276545127744203045L;
	
}
