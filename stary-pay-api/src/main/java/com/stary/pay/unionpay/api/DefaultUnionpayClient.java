package com.stary.pay.unionpay.api;

import com.stary.pay.unionpay.api.rules.UnionpayAccessType;


/**
 * <p>default unionpay client</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-6-21
 * @version 1.3.0
 */
public class DefaultUnionpayClient extends AbstractUnionpayClient {

	private String name;

	public DefaultUnionpayClient(String merId, UnionpayAccessType accessType, String rootCertFile,
			String middleCertFile, String signCertFile, String signCertPwd,
			String encryptCertFile, String secureKey, boolean isTest) {
		super(merId, accessType,rootCertFile, middleCertFile, signCertFile, signCertPwd,
				encryptCertFile, secureKey, isTest);
	}

	public DefaultUnionpayClient(String merId, UnionpayAccessType accessType, String rootCertFile,
			String middleCertFile, String signCertFile, String signCertPwd,
			String encryptCertFile, String secureKey) {
		super(merId, accessType, rootCertFile, middleCertFile, signCertFile, signCertPwd,
				encryptCertFile, secureKey);
	}

	public DefaultUnionpayClient(String merId, UnionpayAccessType accessType, String rootCertFile,
			String middleCertFile, String signCertFile, String encryptCertFile) {
		super(merId, accessType, rootCertFile, middleCertFile, signCertFile, encryptCertFile);
	}

	@Override
	public String getName() {
		return name;
	}

	public DefaultUnionpayClient setName(String name) {
		this.name = name;
		return this;
	}



}
