package com.stary.pay.unionpay.api.request;

import java.util.Map;

import com.stary.pay.unionpay.api.UnionpayConstants;
import com.stary.pay.unionpay.api.UnionpayRequest;
import com.stary.pay.unionpay.api.response.UnionpayOnlineQueryResponse;
import com.stary.pay.unionpay.api.util.UnionpayHashMap;

/**
 * <p>unionpay online query request</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-1
 */
public class UnionpayOnlineQueryRequest implements UnionpayRequest<UnionpayOnlineQueryResponse> {
	/**
	 * 在线网关支付交易状态查询接口
	 */
	private String bizContent;

	public String getBizContent() {
		return bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getFrontUrl() {
		return null;
	}

	@Override
	public void setFrontUrl(String frontUrl) {}

	@Override
	public String getBackUrl() {
		return null;
	}

	@Override
	public void setBackUrl(String backUrl) {}

	@Override
	public Map<String, String> getTextParams() {
		UnionpayHashMap txtParams = new UnionpayHashMap();
		txtParams.put(UnionpayConstants.BIZ_CONTENT, this.bizContent);
		return txtParams;
	}

	@Override
	public Class<UnionpayOnlineQueryResponse> getResponseClass() {
		return UnionpayOnlineQueryResponse.class;
	}

	@Override
	public boolean isCheckSign() {
		return false;
	}

	@Override
	public String getTxnType() {
		return UnionpayConstants.TXNTYPE_ONLINE_QUERY;
	}

	@Override
	public String getTxnSubType() {
		return UnionpayConstants.TXNSUBTYPE_00;
	}

	@Override
	public String getBizType() {
		return UnionpayConstants.BIZTYPE_ONLINE;
	}

	@Override
	public String getChannelType() {
		return UnionpayConstants.CHANNELTYPE_PAGE;
	}

}
