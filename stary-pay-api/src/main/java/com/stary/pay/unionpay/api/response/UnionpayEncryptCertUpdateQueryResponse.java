package com.stary.pay.unionpay.api.response;

import com.stary.pay.unionpay.api.UnionpayResponse;

/**
 * <p>unionpay encrypt cert update query response</p>
 * @author stary {@link stary1993@qq.com}
 * @since 2019-7-9
 */
public class UnionpayEncryptCertUpdateQueryResponse extends UnionpayResponse {

	private static final long serialVersionUID = 5271545127704201041L;	
	/**
	 * 交易类型
	 */
	private String txnType;
	/**
	 * 交易子类
	 */
	private String txnSubType;
	/**
	 * 发送时间，YYYYMMDDhhmmss
	 */
	private String txnTime;
	/**
	 * 签名
	 */
	private String signature;
	/**
	 * 签名方法
	 */
	private String signMethod;

	/**
	 * 签名公钥证书，用RSA签名方式时必选，此域填写银联签名公钥证书。
	 */
	private String signPubKeyCert;
	/**
	 * 接入类型 0：商户直连接入1：收单机构接入2：平台商户接入
	 */
	private String accessType;
	/**
	 * 产品类型
	 */
	private String bizType;
	/**
	 * 商户代码
	 */
	private String merId;
	/**
	 * 敏感信息加密公钥(只有01可用)
	 */
	private String certType;
	/**
	 * 加密公钥证书
	 */
	private String encryptPubKeyCert;
	
	public String getTxnType() {
		return txnType;
	}
	
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	
	public String getTxnSubType() {
		return txnSubType;
	}
	
	public void setTxnSubType(String txnSubType) {
		this.txnSubType = txnSubType;
	}
	
	public String getTxnTime() {
		return txnTime;
	}
	
	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public String getSignMethod() {
		return signMethod;
	}
	
	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}
		
	public String getSignPubKeyCert() {
		return signPubKeyCert;
	}
	
	public void setSignPubKeyCert(String signPubKeyCert) {
		this.signPubKeyCert = signPubKeyCert;
	}
	
	public String getAccessType() {
		return accessType;
	}
	
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	
	public String getBizType() {
		return bizType;
	}
	
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
		
	public String getMerId() {
		return merId;
	}
	
	public void setMerId(String merId) {
		this.merId = merId;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getEncryptPubKeyCert() {
		return encryptPubKeyCert;
	}

	public void setEncryptPubKeyCert(String encryptPubKeyCert) {
		this.encryptPubKeyCert = encryptPubKeyCert;
	}
	
}
