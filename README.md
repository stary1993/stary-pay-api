# stary-pay-api

#### 介绍
1. 包含微信支付产品、微信支付工具，以及微信网页授权相关的API（新增服务商版）。
2. 微信小程序支付（1.2.1版本）、授权登录、解密获取用户信息、手机号信息（1.3.0版本）。
3. 支付宝支付相关对官方的SDK进行了封装。
4. 银联在线网关支付（1.1版本）。

#### 软件架构
由maven构建，添加了`logback-classic`、`httpcore`、`httpclient`、`commons-httpclient`、`fastjson`、`javax.servlet-api`、`bcprov-jdk15on`、`alipay-sdk-java`依赖包。

#### 安装教程

添加依赖（需要手动拉取到本地库）

```
<dependency>
    <groupId>com.stary.pay</groupId>
    <artifactId>stary-pay-api</artifactId>
    <version>1.3.0</version>
</dependency>
```      
	


#### 使用说明

1. 实现并注入微信（IWxpayConfig）/ 支付宝（IAlipayConfig）/ 银联支付（IUnionpayConfig）配置接口Bean

    以下步骤以微信支付为例：
```
	@Bean
	public IWxpayConfig wxpayConfig(){
	    return new IWxpayConfig() {

		@Override
		public String secret() {
		    return "secret";
		}

		@Override
		public String refundNotifyUrl() {
		    return "refundNotifyUrl";
		}

	        @Override
		public String notifyUrl() {
		    return "notifyUrl";
		}

		@Override
		public String mchId() {
		    return "mchId";
		}

	        @Override
		public String subMchId() {
		    return "subMchId";
		}

		@Override
		public String key() {
		    return "key";
		}

		@Override
		public String subKey() {
		    return "subKey";
		}

		@Override
		public String certFile() {
		    return "/certFile";
		}

		@Override
		public String subCertFile() {
		    return "/subCertFile";
		}

		@Override
		public String appid() {
		    return "appid";
		}

		@Override
		public String subAppid() {
		    return "subAppid";
		}
	    };
	}
```

**PS:**  :exclamation: 微信小程序开发支付服务商版情况下，需要在小程序的appid加个前缀 `mini:`。

```
                @Override
		public String subAppid() {
		    return WxpayConstants.MINI + "miniAppid";
		}
```
2. 注入请求客户端Bean

```
         @Bean
         public WxpayClient wxpayClient() throws WxpayApiException {
            InitWxpayClient wxpayClient = new InitWxpayClient();
	    wxpayClient.setWxpayConfig(wxpayConfig());              
	    wxpayClient.setOnlyAuth(false);  // false：支付请求客户端，true：网页授权请求客户端	
	    wxpayClient.setServiceProvider(true); // false：普通商户，true：服务商
            wxpayClient.setName("wxpay-01"); // 客户端名称
	    return wxpayClient.build();
         }
```

3. 注入Wxpay业务Bean

```
        @Bean
        public Wxpay wxpay() throws WxpayApiException {
            return new Wxpay(wxpayConfig(), wxpayClient());
        }
```

4. 自动装配Wxpay

```
        @Autowired
        private Wxpay wxpay;
```
以统一下单为例：


```
        WxpayUnifiedorder unifiedorder = new WxpayUnifiedorder();
        unifiedorder.setBody("测试商品名称");
        unifiedorder.setOut_trade_no("DD201907101400845");
        unifiedorder.setTotal_fee("1");
        unifiedorder.setTrade_type(WxpayTradeType.NATIVE);
        WxpayUnifiedorderResponse  response = wxpay.unifiedorder(unifiedorder);
        if (response.isSuccess()) { // 微信业务请求成功			
	   if(WxpayConstants.SUCCESS.equals(response.getResultCode())) {
             // 统一下单成功
	     // 自己的业务代码
	    }
        }
```


    

#### 参与贡献

1. 微信支付官方文档：[https://pay.weixin.qq.com/wiki/doc/api/index.html](https://pay.weixin.qq.com/wiki/doc/api/index.html)
2. 微信公众号开发（网页授权）官方文档：[https://mp.weixin.qq.com/wiki](https://mp.weixin.qq.com/wiki)
3. 微信小程序开发官方文档：[https://developers.weixin.qq.com/miniprogram/dev/framework/](https://developers.weixin.qq.com/miniprogram/dev/framework/)
4. 支付宝支付API官方文档：[https://docs.open.alipay.com/api](https://docs.open.alipay.com/api)
5. 银联支付API官方文档：[https://open.unionpay.com/tjweb/api/list](https://open.unionpay.com/tjweb/api/list)


